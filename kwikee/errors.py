from __future__ import absolute_import, division, print_function, unicode_literals


class UnknownAttribute(Warning):

    pass


class ArgumentError(Exception):

    pass
