from __future__ import absolute_import, division, print_function, unicode_literals

from future import standard_library

standard_library.install_aliases()

from builtins import *

from http.client import HTTPResponse

import base64
from datetime import datetime, date
from io import BytesIO
from urllib.request import HTTPError, Request, urlopen
from zipfile import ZipFile

try:
    from typing import Iterator, Optional, Union
except ImportError:
    pass

from kwikee.elements import KwikeeData, Service, Init, Incr, IncrRequest, SOAPElement, ReplicationFileList

HOST = 'api.kwikeesystems.com'
VERSION = '1.5'
XSD = 'https://%s/api/v%s/kwikee.xsd' % (HOST, VERSION)
ORIGIN = 'https://%s/v%s/' % (HOST, VERSION)


class Kwikee:

    def __init__(
        self,
        user,  # type: str
        password  # type: str
    ):
        self.user = user
        self.password = password

    def request(
        self,
        url=None,  # type: Optional[str]
        data=None,  # type: Optional[str]
        echo=False  # type: bool
    ):
        # type: (...) -> HTTPResponse
        if not self.user and self.password:
            raise PermissionError(
                'Authentication information is required'
            )
        if isinstance(data, SOAPElement):
            data = str(data)
        if isinstance(data, str):
            data = bytes(data, 'utf-8')
        request = Request(
            url,
            headers={
                'Host': HOST,
                'Authorization': (
                    'Basic ' + str(
                        base64.b64encode(
                            bytes(
                                '%s:%s' % (
                                    self.user,
                                    self.password
                                ),
                                'utf-8'
                            )
                        ),
                        'ascii'
                    )
                )
            },
            data=data
        )

        def request_text():
            return (
                ('\n%s: %s\n' % (request.get_method(), url)) +
                '\n'.join(
                    '%s: %s' % (k, v)
                    for k, v in request.header_items()
                ) + (
                    ('\n' + str(request.data, encoding='utf-8'))
                    if request.data is not None
                    else ''
                )
            )

        if echo:
            print(request_text())
        try:
            response = urlopen(request)
        except HTTPError as e:
            response_data = str(e.file.read(), 'utf-8')
            e.msg = (
                'Request:\n' +
                request_text() +
                '\n\nResponse:\n' +
                response_data +
                '\n\n' +
                e.msg
            )
            raise e
        return response

    def init(
        self,
        echo=False  # type: bool
    ):
        # type: (...) -> Iterator[KwikeeData]
        response = self.request(
            url='%s%s/xml' % (ORIGIN, 'init'),
            data=KwikeeData(
                service=Service(
                    init=Init(
                        request=''
                    )
                )
            ),
            echo=echo
        )
        file_list = KwikeeData(response).service.init.response.file_list
        if file_list is not None:
            for file in file_list.file:
                response = self.request(file.file_url)
                zf = ZipFile(BytesIO(response.read()))
                for fn in zf.namelist():
                    kd = KwikeeData(str(zf.open(fn).read(), encoding='utf-8'))
                    kd.response = response
                    yield kd

    def incr(
        self,
        start_date,  # type: Union[date, datetime]
        end_date=None,  # type: Optional[Union[date, datetime]]
        echo=False  # type: bool
    ):
        # type: (...) -> Iterator[KwikeeData]
        if isinstance(start_date, date):
            start_date = datetime(start_date.year, start_date.month, start_date.day)
        if isinstance(end_date, date):
            end_date = datetime(end_date.year, end_date.month, end_date.day)
        response = self.request(
            url='%s%s/xml' % (ORIGIN, 'incr'),
            data=KwikeeData(
                service=Service(
                    incr=Incr(
                        request=IncrRequest(
                            start_date=start_date,
                            # Note: the `end_date` parameter causes an empty return set to be returned, so instead of
                            # of passing this parameter to the `IncrRequest`, we exclude the files generated after
                            # this date when iterating over the zip files.
                        )
                    )
                )
            ),
            echo=echo
        )
        file_list = KwikeeData(response).service.incr.response.file_list
        if file_list is not None:
            for file in file_list.file:
                if (
                    (end_date is not None) and
                    file.generated_date > end_date
                ):
                    continue
                response = self.request(file.file_url)
                zf = ZipFile(BytesIO(response.read()))
                for fn in zf.namelist():
                    kd = KwikeeData(str(zf.open(fn).read(), encoding='utf-8'))
                    kd.response = response
                    yield kd
