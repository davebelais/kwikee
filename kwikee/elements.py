"""
This module defines data structures to represent Kwikee XML messages.
"""
from __future__ import absolute_import, division, print_function, unicode_literals

import re
from base64 import b64encode, b64decode
from collections import OrderedDict
from copy import deepcopy
from decimal import Decimal, InvalidOperation
from http.client import HTTPResponse
from itertools import chain
from time import timezone
from xml.etree.ElementTree import XML, Element, tostring, ParseError

from builtins import *
from datetime import datetime, date, timedelta

from kwikee.errors import UnknownAttribute

try:
    from typing import Sequence, Optional, Set, Dict, Union
except ImportError:
    pass

NAME_SPACES = OrderedDict()


_DATE_OFFSET_RE = re.compile(r'([\-+])(\d\d):?(\d\d)$')
_DATE_DECIMAL_RE = re.compile(r'(\.\d\d\d\d\d\d)\d+')


def str2datetime(s):
    # type: (str) -> datetime
    """
    Convert an XML `dateTime` string to a python `datetime` object.

    >>> print(str2datetime('2001-10-26T21:32:52+02:00'))
    2001-10-26 19:32:52

    >>> print(str2datetime('2016-03-28T23:33:41.3116627-0500'))
    2016-03-29 04:33:41.311662
    """
    dtf = "%Y-%m-%d"
    if 'T' in s:
        dtf += "T%H:%M:%S"
        if '.' in s:
            dtf += '.%f'
            s = _DATE_DECIMAL_RE.sub(
                r'\1',
                s
            )
    offset = None
    m = _DATE_OFFSET_RE.search(s)
    if m:
        # Convert the date/time to local time
        local_offset = -(timezone/(60 ** 2))
        offset_direction, hours, minutes = m.groups()
        if offset_direction == '-':
            offset = timedelta(hours=local_offset + int(hours or 0), minutes=int(minutes or 0))
        else:
            offset = timedelta(hours=local_offset - int(hours or 0), minutes=-int(minutes or 0))
        s = _DATE_OFFSET_RE.sub(
            '',
            s
        )
    dt = datetime.strptime(
        s,
        dtf
    )
    if offset is not None:
        dt = dt - offset
    return dt


def str2date(s):
    # type: (str) -> date
    """
    Convert an XML `date` string to a python `date` object.

    >>> print(str2date('2001-10-26'))
    2001-10-26
    """
    d = str2datetime(s)
    return date(
        year=d.year,
        month=d.month,
        day=d.day
    )


def datetime2str(dt):
    # type: (datetime) -> str
    s = dt.strftime('%Y-%m-%dT%H:%M:%S%z')
    return s


def date2str(d):
    # type: (date) -> str
    s = d.strftime('%Y-%m-%d')
    return s


TYPES_BASES = OrderedDict()


def bases(t):
    # type: (type) -> Set[type]
    """
    Retrieves a set containing all classes (`type` instances) from which the given class inherits.

        :param t: An instance type.

        :return: A set containing all types from which this type inherits (directly and indirectly).
    """
    if t not in TYPES_BASES:
        bs = set()
        for b in t.__bases__:
            for bb in bases(b):  # type: Set[type]
                bs.add(bb)
            bs.add(b)
        TYPES_BASES[t] = bs
    return TYPES_BASES[t]


def space_name(s):
    # type: (str) -> str
    """
    This function returns the un-qualified name of an XML element or attribute.

    :param s:
        An tag or attribute name.
    :return:
        The unqualified name of the XML element or attribute.
    :rtype:
        str
    """
    return re.match(
        r'^(?:\{(.*?)\})?(.*)$',
        s
    ).groups()


class SOAPElement(object):

    """
    This is a base class for building objects to represent SOAP XML elements.

    Child classes should each have the following properties:

        elements_properties:

            This should be an `OrderedDict` object which maps sub-element XML tag names (not including a name space) to
            a tuple containing the corresponding property name + type.

        xmlns:

            The full URL of the element's default name space.

    Child classes should initialize each of the properties from their static `elements_properties` property in their
    `__init__` method with a value of `None` (for properties corresponding to elements which can occur only once) or an
    empty list (for properties which correspond to elements which can occur more than once).
    """

    elements_properties = OrderedDict([])  # type: Dict[str, Sequence[str, type]]

    xmlns = None

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]],
        tag=None  # type: Optional[str]
    ):
        # type: (...) -> None
        """
        Initializes a `SOAPElement` instance, optionally deriving property values from provided XML data.

        :param xml:

            (optional) An instance of `str` or `xml.etree.ElementTree.Element` which represents an XML element, or an 
            `HTTPResponse` object containing the same information.
            If provided, the tag name of the given element will override the `tag` parameter below.

        :param tag:

            (optional) The tag name of the element (not including the name space). This is needed only if no
            `xml` is provided.
        """
        self._string = None
        self._element = None
        self.response = None
        self.tag = tag or self.__class__.__name__
        if xml is not None:
            namespace = None
            if isinstance(xml, str):
                self._string = xml
                try:
                    self._element = XML(xml)
                except ParseError as e:
                    e.args = tuple(chain(
                        (
                            ((e.args[0] + '\n\n') if e.args else '') +
                            xml
                        ),
                        e.args[1:] if e.args and len(e.args) > 1 else tuple()
                    ))
                    raise e
                namespace, self.tag = space_name(self._element.tag)
            elif isinstance(xml, Element):
                self._element = xml
                namespace, self.tag = space_name(self._element.tag)
            elif hasattr(xml, 'read'):
                self.response = xml
                self._string = str(xml.read(), 'utf-8', errors='ignore')
                try:
                    self._element = XML(self._string)
                except ParseError as e:
                    e.args = tuple(chain(e.args, (self._string,)))
                    raise e
                namespace, self.tag = space_name(self._element.tag)
            else:
                raise TypeError(xml)
            if self.elements_properties is not None:
                namespaces_prefixes = {}
                for a, p_t in self.elements_properties.items():
                    if a[:7] == '@xmlns:':
                        prefix = a[7:]
                        p, t = p_t
                        v = getattr(self, p)
                        if v is not None:
                            namespaces_prefixes[prefix] = v
                            if v == namespace:
                                self.tag = '%s:%s' % (prefix, self.tag)
                tags_elements_properties_types = []
                for a, v in self._element.attrib.items():
                    a_ns, a = space_name(a)
                    if a_ns in namespaces_prefixes:
                        nsp_a = '%s:%s' % (namespaces_prefixes[a_ns], a)
                        if nsp_a in self.elements_properties:
                            a = nsp_a
                    tn = '@' + a
                    if tn in self.elements_properties:
                        p, t = self.elements_properties[tn]
                        tags_elements_properties_types.append((tn, v, p, t))
                    else:
                        raise UnknownAttribute('`%s` is not a recognized attribute of `%s`:\n\n%s' % (
                            a,
                            self.__class__.__name__,
                            (self._string or tostring(self._element, encoding='unicode'))
                        ))
                v = self._element.text
                if v:
                    if '.' in self.elements_properties:
                        p, t = self.elements_properties['.']
                        tags_elements_properties_types.append(
                            ('.', v, p, t)
                        )
                    elif v.strip():
                        raise UnknownAttribute(
                            'No mapping was found for the text content of a `%s` element.' % self.tag
                        )
                for element in self._element:
                    ns, tn = space_name(element.tag)
                    if tn in self.elements_properties:
                        p, t = self.elements_properties[tn]
                        tags_elements_properties_types.append((tn, element, p, t))
                    else:
                        success = True
                        for e_a, e_v in element.attrib.items():
                            ns_a, e_a = space_name(e_a)
                            tn_a = '%s@%s' % (tn, e_a)
                            if tn_a in self.elements_properties:
                                p, t = self.elements_properties[tn_a]
                                tags_elements_properties_types.append((tn_a, e_v, p, t))
                            else:
                                success = False
                                break
                        for sub_element in element:
                            se_ns, se_tn = space_name(sub_element.tag)
                            se_tn = '%s/%s' % (tn, se_tn)
                            if se_tn in self.elements_properties:
                                p, t = self.elements_properties[se_tn]
                                tags_elements_properties_types.append((se_tn, sub_element, p, t))
                            else:
                                success = False
                                break
                        if not success:
                            if '*' in self.elements_properties:
                                p, t = self.elements_properties['*']
                                if SOAPElement in bases(t):
                                    tags_elements_properties_types.append((tn, element, p, t))
                                else:
                                    raise UnknownAttribute(
                                        '`%s` is not a recognized child element of `%s`' % (tn, self.__class__.__name__)
                                    )
                            else:
                                raise UnknownAttribute(
                                    '`%s` is not a recognized child element of `%s`' % (tn, self.__class__.__name__)
                                )
                for tn, v, p, t in tags_elements_properties_types:
                    if isinstance(t, (tuple, list)):
                        t = t[0] if t else None
                        if SOAPElement in bases(t):
                            if isinstance(v, Element):
                                getattr(self, p).append(t(v))
                            else:
                                raise ValueError(v)
                        else:
                            if t is Element:
                                if not isinstance(v, (Element, SOAPElement)):
                                    raise ValueError(v)
                                getattr(self, p).append(v)
                            else:
                                if isinstance(v, Element):
                                    v = v.text
                                if t is bool:
                                    getattr(self, p).append(
                                        True if v.lower().strip() in ('true', '1', 'yes', 'y')
                                        else False
                                    )
                                elif t is int:
                                    try:
                                        v = int(Decimal(v))
                                    except InvalidOperation as e:
                                        error_message = (
                                            '%s.%s: %s could not be cast as an integer\n\n%s' % (
                                                self.__class__.__name__,
                                                p,
                                                repr(v),
                                                self._string or tostring(self._element)
                                            ) + (
                                                (
                                                    '\n\n' + (
                                                        e.args[0]
                                                        if isinstance(e.args[0], str)
                                                        else repr(e.args[0])
                                                    )
                                                ) if e.args else ''
                                            )
                                        )
                                        e.args = tuple(chain(
                                            (error_message,),
                                            e.args[1:] if e.args else tuple()
                                        ))
                                        raise e
                                    getattr(self, p, v).append(v)
                                elif t is date:
                                    getattr(self, p).append(str2date(v))
                                elif t is datetime:
                                    getattr(self, p).append(str2datetime(v))
                                elif t is bytes:
                                    getattr(self, p).append(b64decode(v))
                                else:
                                    getattr(self, p).append(t(v))
                    else:
                        if SOAPElement in bases(t):
                            if isinstance(v, (Element, SOAPElement)):
                                setattr(self, p, t(v))
                            else:
                                raise ValueError(
                                    '%s is not a valid value for `%s.%s`' % (
                                        repr(v),
                                        self.__class__.__name__,
                                        p
                                    )
                                )
                        else:
                            if t is Element:
                                if not isinstance(v, Element):
                                    raise ValueError(v)
                                setattr(self, p, v)
                            else:
                                if isinstance(v, Element):
                                    v = v.text
                                if isinstance(v, str):
                                    v = v.strip()
                                    if v == '':
                                        continue
                                if t is bool:
                                    setattr(
                                        self,
                                        p,
                                        True if v.lower().strip() in ('true', '1', 'yes', 'y') else False
                                    )
                                elif t is int:
                                    try:
                                        v = int(Decimal(v))
                                    except InvalidOperation as e:
                                        error_message = (
                                            '%s.%s: %s could not be cast as an integer\n\n%s' % (
                                                self.__class__.__name__,
                                                p,
                                                repr(v),
                                                self._string or tostring(self._element)
                                            ) + (
                                                (
                                                    '\n\n' + (
                                                        e.args[0]
                                                        if isinstance(e.args[0], str)
                                                        else repr(e.args[0])
                                                    )
                                                ) if e.args else ''
                                            )
                                        )
                                        e.args = tuple(chain(
                                            (error_message,),
                                            e.args[1:] if e.args else tuple()
                                        ))
                                        raise e
                                    setattr(self, p, v)
                                elif t is date:
                                    setattr(self, p, str2date(v))
                                elif t is datetime:
                                    setattr(self, p, str2datetime(v))
                                elif t is bytes:
                                    setattr(self, p, b64decode(v))
                                else:
                                    try:
                                        setattr(self, p, t(v))
                                    except TypeError as e:
                                        e.args = tuple(
                                            [
                                                '\n%s: %s' % (t.__name__, repr(e.args[0])) + (
                                                    '\n' + e.args[0] if e.args else ''
                                                )
                                            ] + (
                                                list(e.args[1:])
                                                if len(e.args) > 1
                                                else []
                                            )
                                        )
                                        raise e

    def __str__(self):
        # type: () -> str
        """
        Casting instances of this class as `str` objects returns a normalized XML representation of the object, suitable
        for comparison, hashing, or passing in a SOAP request.

        :return:

            A normalized XML representation of the `SOAPElement` instance.
        """
        e = self.tag
        attributes_values = []
        if self.xmlns is not None:
            attributes_values.append(('xmlns', self.xmlns))
        for a, p_t in self.elements_properties.items():
            if a and a[0] == '@':
                a = a[1:]
                p, t = p_t
                vs = getattr(self, p)
                if vs is None:
                    continue
                if isinstance(t, (tuple, list)):
                    if t:
                        t = t[0]
                    else:
                        t = None
                else:
                    vs = [vs]
                for v in vs:
                    if not isinstance(v, t):
                        raise TypeError(
                            '%s is not an instance of `%s` (encountered while parsing `%s.%s`)' % (
                                repr(v),
                                t.__name__,
                                self.__class__.__name__,
                                p
                            )
                        )
                    if isinstance(v, datetime):
                        v = datetime2str(v)
                    elif isinstance(v, date):
                        v = date2str(v)
                    elif isinstance(v, bool):
                        v = 'true' if v else 'false'
                    elif isinstance(v, bytes):
                        v = str(b64encode(v), encoding='ascii')
                    elif isinstance(v, str):
                        if '"' in v:
                            v = v.replace('"', r'\"')
                        if '&' in v:
                            v = v.replace('&', '&amp;')
                        if '<' in v:
                            v = v.replace('<', '&lt;')
                        if '>' in v:
                            v = v.replace('>', '&gt;')
                    else:
                        v = str(v)
                    attributes_values.append((a, v))
        s = [
            '<%s%s>' % (
                self.tag,
                ''.join(
                    ' %s="%s"' % (a, v)
                    for a, v in attributes_values
                )
            )
        ]
        for tn, p_t in self.elements_properties.items():
            if tn and (tn[0] == '@' or tn == '*'):
                continue
            p, t = p_t
            vs = getattr(self, p)
            if isinstance(t, (tuple, list)):
                if t:
                    t = t[0]
                else:
                    t = None
            else:
                vs = [] if vs is None else [vs]
            if vs is None:
                raise ValueError(
                    '`None` is not a valid value for `%s.%s`' % (
                        self.__class__.__name__,
                        p
                    )
                )
            for v in vs:
                if v is not None:
                    if not (
                        (isinstance(v, SOAPElement) and t is Element) or
                        isinstance(v, t)
                    ):
                        raise TypeError(
                            '`%s.%s`: `%s` is not an instance of `%s`' % (
                                self.__class__.__name__,
                                p,
                                repr(v),
                                t.__name__
                            )
                        )
                if isinstance(v, SOAPElement):
                    if tn == '.':
                        raise ValueError(
                            '`%s.%s`: A `SOAPElement` cannot be attributed to the text content of an element.' % (
                                self.__class__.__name__,
                                p
                            )
                        )
                else:
                    if isinstance(v, Element):
                        v = tostring(v, encoding='unicode')
                    elif isinstance(v, datetime):
                        v = datetime2str(v)
                    elif isinstance(v, date):
                        v = date2str(v)
                    elif isinstance(v, bool):
                        v = 'true' if v else 'false'
                    elif isinstance(v, bytes):
                        v = str(b64encode(v), encoding='ascii')
                    elif isinstance(v, str):
                        if '&' in v:
                            v = v.replace('&', '&amp;')
                        if '<' in v:
                            v = v.replace('<', '&lt;')
                        if '>' in v:
                            v = v.replace('>', '&gt;')
                    else:
                        v = str(v)
                if tn == '.':
                    s.append(v)
                else:
                    for tn in reversed(tn.split('/')):
                        if isinstance(v, SOAPElement):
                            v = str(v)
                        elif '@' in tn:
                            tn, ta = tn.split('@')
                            v = '<%(tn)s %(ta)s="%(v)s" />' % dict(
                                tn=tn,
                                ta=ta,
                                v=v
                            )
                        else:
                            v = '<%(tn)s>%(v)s</%(tn)s>' % dict(
                                tn=tn,
                                v=v
                            )
                    s.append(v)
        if '*' in self.elements_properties:
            for se in self._element:
                ns, tn = space_name(se.tag)
                if tn not in self.elements_properties:
                    s.append(str(se))
        s.append('</%s>' % e)
        return ''.join(s)

    def __bool__(self):
        # type: () -> bool
        return True

    def __eq__(self, other):
        # type: (object) -> bool
        return (
            hasattr(other, '__class__') and
            (self.__class__ is other.__class__) and
            str(self) == str(other)
        )

    def __ne__(self, other):
        # type: (object) -> bool
        return (
            False if self == other
            else True
        )

    def __hash__(self):
        return hash(str(self))

    def __copy__(self):
        nse = self.__class__(self._string or self._element)
        nse.response = self.response
        for p, t in self.elements_properties.values():
            setattr(nse, p, getattr(self, p))
        return nse

    def __deepcopy__(
        self,
        memo=None  # type: Optional[dict]
    ):
        nse = self.__class__(
            self._string or
            deepcopy(self._element) if self._element is not None
            else None
        )
        nse.response = deepcopy(self.response, memo=memo)
        for p, t in self.elements_properties.values():
            setattr(nse, p, deepcopy(getattr(self, p), memo=memo))
        return nse


class QueryList(SOAPElement):
    """
    Properties:
    
        - query_field (str)        
        - query_value (Sequence[str])
    """

    elements_properties = OrderedDict([
        ('query_field', ('query_field', str)),
        ('query_value', ('query_value', (str,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='query_list',  # type: str
        query_field=None,  # type: Optional[str]
        query_value=None,  # type: Optional[Sequence[str]]
    ):
        self.query_field = query_field  # type: Optional[str]
        self.query_value = query_value or []  # type: Sequence[str]
        super().__init__(xml=xml, tag=tag)


class QueryRequest(SOAPElement):
    """
    Properties:
    
        - query_list (QueryList)
    """

    elements_properties = OrderedDict([
        ('query_list', ('query_list', QueryList)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='request',  # type: str
        query_list=None,  # type: Optional[QueryList]
    ):
        self.query_list = query_list  # type: Optional[QueryList]
        super().__init__(xml=xml, tag=tag)


class ResponseInfo(SOAPElement):
    """
    Properties:
    
        - code (str)
        
        - description (str)
        
        - details (str)
    """

    elements_properties = OrderedDict([
        ('code', ('code', str)),
        ('description', ('description', str)),
        ('details', ('details', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='response',  # type: str
        code=None,  # type: Optional[str]
        description=None,  # type: Optional[str]
        details=None,  # type: Optional[str]
    ):
        self.code = code  # type: Optional[str]
        self.description = description  # type: Optional[str]
        self.details = details  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class QueryResponse(SOAPElement):
    """
    Properties:
    
        - transaction_id (str)        
        - response_info (ResponseInfo)
    """

    elements_properties = OrderedDict([
        ('transaction_id', ('transaction_id', str)),
        ('response_info', ('response_info', ResponseInfo)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='response',  # type: str
        transaction_id=None,  # type: Optional[str]
        response_info=None,  # type: Optional[ResponseInfo]
    ):
        self.transaction_id = transaction_id  # type: Optional[str]
        self.response_info = response_info  # type: Optional[ResponseInfo]
        super().__init__(xml=xml, tag=tag)


class Query(SOAPElement):
    """
    Properties:
    
        - request (QueryRequest)
        
        - response (QueryResponse)
    """

    elements_properties = OrderedDict([
        ('request', ('request', QueryRequest)),
        ('response', ('response', QueryResponse)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='service',  # type: str
        request=None,  # type: Optional[QueryRequest]
        response=None,  # type: Optional[QueryResponse]
    ):
        self.request = request  # type: Optional[QueryRequest]
        self.response = response  # type: Optional[QueryResponse]
        super().__init__(xml=xml, tag=tag)


class StatusRequest(SOAPElement):
    """
    Properties:
    
        - transaction_id (str)
    """

    elements_properties = OrderedDict([
        ('transaction_id', ('transaction_id', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='request',  # type: str
        transaction_id=None,  # type: Optional[str]
    ):
        self.transaction_id = transaction_id  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class StatusResponse(SOAPElement):
    """
    Properties:
    
        - response_info (ResponseInfo)
    """

    elements_properties = OrderedDict([
        ('response_info', ('response_info', ResponseInfo)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='response',  # type: str
        response_info=None,  # type: Optional[ResponseInfo]
    ):
        self.response_info = response_info  # type: Optional[ResponseInfo]
        super().__init__(xml=xml, tag=tag)


class Status(SOAPElement):
    """
    Properties:
    
        - request (StatusRequest)
        
        - response (StatusResponse)
    """

    elements_properties = OrderedDict([
        ('request', ('request', StatusRequest)),
        ('response', ('response', StatusResponse)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='service',  # type: str
        request=None,  # type: Optional[StatusRequest]
        response=None,  # type: Optional[StatusResponse]
    ):
        self.request = request  # type: Optional[StatusRequest]
        self.response = response  # type: Optional[StatusResponse]
        super().__init__(xml=xml, tag=tag)


class ReplicationFile(SOAPElement):
    """
    Properties:
    
        - file_url (str)
        - generated_date (datetime)
    """

    elements_properties = OrderedDict([
        ('file_url', ('file_url', str)),
        ('generated_date', ('generated_date', datetime)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='file',  # type: str
        file_url=None,  # type: Optional[str]
        generated_date=None,  # type: Optional[datetime]
    ):
        self.file_url = file_url  # type: Optional[str]
        self.generated_date = generated_date  # type: Optional[datetime]
        super().__init__(xml=xml, tag=tag)


class ReplicationFileList(SOAPElement):
    """
    Properties:
    
        - file (ReplicationFile)
    """

    elements_properties = OrderedDict([
        ('file', ('file', (ReplicationFile,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='file_list',  # type: str
        file=None,  # type: Sequence[ReplicationFile]
    ):
        self.file = file or []  # type: Sequence[ReplicationFile]
        super().__init__(xml=xml, tag=tag)


class InitResponse(SOAPElement):
    """
    Properties:
    
        - file_list (ReplicationFileList)
        - response_info (ResponseInfo)
    """

    elements_properties = OrderedDict([
        ('file_list', ('file_list', ReplicationFileList)),
        ('response_info', ('response_info', ResponseInfo)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='response',  # type: str
        file_list=None,  # type: Optional[ReplicationFileList]
        response_info=None,  # type: Optional[ResponseInfo]
    ):
        self.file_list = file_list  # type: Optional[ReplicationFileList]
        self.response_info = response_info  # type: Optional[ResponseInfo]
        super().__init__(xml=xml, tag=tag)


class Init(SOAPElement):
    """
    Properties:
    
        - request (str)
        
        - response (InitResponse)
    """

    elements_properties = OrderedDict([
        ('request', ('request', str)),
        ('response', ('response', InitResponse)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='init',  # type: str
        request=None,  # type: Optional[str]
        response=None,  # type: Optional[InitResponse]
    ):
        self.request = request  # type: Optional[str]
        self.response = response  # type: Optional[InitResponse]
        super().__init__(xml=xml, tag=tag)


class IncrResponse(InitResponse):

    pass


class IncrRequest(SOAPElement):
    """
    Properties:
    
        - start_date (datetime)
        
        - end_date (datetime)
    """

    elements_properties = OrderedDict([
        ('start_date', ('start_date', datetime)),
        ('end_date', ('end_date', datetime)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='request',  # type: str
        start_date=None,  # type: Optional[datetime]
        end_date=None,  # type: Optional[datetime]
    ):
        self.start_date = start_date  # type: Optional[datetime]
        self.end_date = end_date  # type: Optional[datetime]
        super().__init__(xml=xml, tag=tag)


class Incr(SOAPElement):
    """
    Properties:
    
        - request (IncrRequest)      
        - response (IncrResponse)
    """

    elements_properties = OrderedDict([
        ('request', ('request', IncrRequest)),
        ('response', ('response', IncrResponse)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='incr',  # type: str
        request=None,  # type: Optional[IncrRequest]
        response=None,  # type: Optional[IncrResponse]
    ):
        self.request = request  # type: Optional[IncrRequest]
        self.response = response  # type: Optional[IncrResponse]
        super().__init__(xml=xml, tag=tag)


class ProductRequest(SOAPElement):
    """
    Properties:
    
        - brand_name (str)
        - description (str)    
        - manufacturer_name (str)    
        - vendor_id (str)
        - retailer_contract_flag (bool)

    """

    elements_properties = OrderedDict([
        ('brand_name', ('brand_name', str)),
        ('description', ('description', str)),
        ('manufacturer_name', ('manufacturer_name', str)),
        ('vendor_id', ('vendor_id', str)),
        ('retailer_contract_flag', ('retailer_contract_flag', bool)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='product_request',  # type: str
        brand_name=None,  # type: Optional[str]
        description=None,  # type: Optional[str]
        manufacturer_name=None,  # type: Optional[str]
        vendor_id=None,  # type: Optional[str]
        retailer_contract_flag=None,  # type: Optional[bool]
    ):
        self.brand_name = brand_name  # type: Optional[str]
        self.description = description  # type: Optional[str]
        self.manufacturer_name = manufacturer_name  # type: Optional[str]
        self.vendor_id = vendor_id  # type: Optional[str]
        self.retailer_contract_flag = retailer_contract_flag  # type: Optional[bool]
        super().__init__(xml=xml, tag=tag)


class ProductRequestList(SOAPElement):
    """
    Properties:
    
        - product_request (Sequence[ProductRequest])
    """

    elements_properties = OrderedDict([
        ('product_request', ('product_request', (ProductRequest,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='product_request_list',  # type: str
        product_request=None,  # type: Optional[Sequence[ProductRequest]]
    ):
        self.product_request = product_request or []  # type: Sequence[ProductRequest]
        super().__init__(xml=xml, tag=tag)


class VendorAccount(SOAPElement):
    """
    Properties:
    
        - vendor_id (str)
        - company_name (str)    
        - address_1 (str)    
        - address_2 (str)    
        - city (str)
        - state_province (str)
        - zip_postal (str)
        - country_id (str): An ISO alpha-2 country code.
        - email_address (str)
        - first_name (str)
        - middle_name (str)
        - last_name (str)
        - phone_number (str)
        - phone_extension (str)
        
    """

    elements_properties = OrderedDict([
        ('vendor_id', ('vendor_id', str)),
        ('company_name', ('company_name', str)),
        ('address_1', ('address_1', str)),
        ('address_2', ('address_2', str)),
        ('city', ('city', str)),
        ('state_province', ('state_province', str)),
        ('zip_postal', ('zip_postal', str)),
        ('country_id', ('country_id', str)),
        ('email_address', ('email_address', str)),
        ('first_name', ('first_name', str)),
        ('middle_name', ('middle_name', str)),
        ('last_name', ('last_name', str)),
        ('phone_number', ('phone_number', str)),
        ('phone_extension', ('phone_extension', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='vendor_account',  # type: str
        vendor_id=None,  # type: Optional[str]
        company_name=None,  # type: Optional[str]
        address_1=None,  # type: Optional[str]
        address_2=None,  # type: Optional[str]
        city=None,  # type: Optional[str]
        state_province=None,  # type: Optional[str]
        zip_postal=None,  # type: Optional[str]
        country_id=None,  # type: Optional[str]
        email_address=None,  # type: Optional[str]
        first_name=None,  # type: Optional[str]
        middle_name=None,  # type: Optional[str]
        last_name=None,  # type: Optional[str]
        phone_number=None,  # type: Optional[str]
        phone_extension=None,  # type: Optional[str]
    ):
        self.vendor_id = vendor_id  # type: Optional[str]
        self.company_name = company_name  # type: Optional[str]
        self.address_1 = address_1  # type: Optional[str]
        self.address_2 = address_2  # type: Optional[str]
        self.city = city  # type: Optional[str]
        self.state_province = state_province  # type: Optional[str]
        self.zip_postal = zip_postal  # type: Optional[str]
        self.country_id = country_id  # type: Optional[str]
        self.email_address = email_address  # type: Optional[str]
        self.first_name = first_name  # type: Optional[str]
        self.middle_name = middle_name  # type: Optional[str]
        self.last_name = last_name  # type: Optional[str]
        self.phone_number = phone_number  # type: Optional[str]
        self.phone_extension = phone_extension  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class VendorAccountList(SOAPElement):
    """
    Properties:
    
        - vendor_account (Sequence[VendorAccount])
    """

    elements_properties = OrderedDict([
        ('vendor_account', ('vendor_account', (VendorAccount,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='vendor_account_list',  # type: str
        vendor_account=None,  # type: Optional[Sequence[VendorAccount]]
    ):
        self.vendor_account = vendor_account or []  # type: Sequence[VendorAccount]
        super().__init__(xml=xml, tag=tag)


class UpdateRequest(SOAPElement):
    """
    Properties:
    
        - product_request_list (ProductRequestList)
        
        - vendor_account_list (VendorAccountList)
    """

    elements_properties = OrderedDict([
        ('product_request_list', ('product_request_list', ProductRequestList)),
        ('vendor_account_list', ('vendor_account_list', VendorAccountList)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='request',  # type: str
        product_request_list=None,  # type: Optional[ProductRequestList]
        vendor_account_list=None,  # type: Optional[VendorAccountList]
    ):
        self.product_request_list = product_request_list  # type: Optional[ProductRequestList]
        self.vendor_account_list = vendor_account_list  # type: Optional[VendorAccountList]
        super().__init__(xml=xml, tag=tag)


class UpdateResponse(QueryResponse):

    pass


class Update(SOAPElement):
    """
    Properties:
    
        - request (UpdateRequest)
        
        - response (UpdateResponse)
    """

    elements_properties = OrderedDict([
        ('request', ('request', UpdateRequest)),
        ('response', ('response', UpdateResponse)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='update',  # type: str
        request=None,  # type: Optional[UpdateRequest]
        response=None,  # type: Optional[UpdateResponse]
    ):
        self.request = request  # type: Optional[UpdateRequest]
        self.response = response  # type: Optional[UpdateResponse]
        super().__init__(xml=xml, tag=tag)


class Service(SOAPElement):
    """
    Properties:
    
        - query (Query)
        
        - status (Status)
        
        - init (Init)
        
        - incr (Incr)
        
        - update (Update)
    """

    elements_properties = OrderedDict([
        ('query', ('query', Query)),
        ('status', ('status', Status)),
        ('init', ('init', Init)),
        ('incr', ('incr', Incr)),
        ('update', ('update', Update)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='service',  # type: str
        query=None,  # type: Optional[Query]
        status=None,  # type: Optional[Status]
        init=None,  # type: Optional[Init]
        incr=None,  # type: Optional[Incr]
        update=None,  # type: Optional[Update]
    ):
        self.query = query  # type: Optional[Query]
        self.status = status  # type: Optional[Status]
        self.init = init  # type: Optional[Init]
        self.incr = incr  # type: Optional[Incr]
        self.update = update  # type: Optional[Update]
        super().__init__(xml=xml, tag=tag)


class Brand(SOAPElement):
    """
    Properties:
    
        - name (str)
        - id (int)
    """

    elements_properties = OrderedDict([
        ('name', ('name', str)),
        ('id', ('id', int)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='brand',  # type: str
    ):
        self.name = None  # type: Optional[str]
        self.id = None  # type: Optional[int]
        super().__init__(xml=xml, tag=tag)


class Manufacturer(Brand):
    """
    Properties:
    
        - name (str)
        - id (int)
    """

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='manufacturer',  # type: str
    ):
        super().__init__(xml=xml, tag=tag)


class KwikeeNutrition(SOAPElement):
    """
    Properties:    
        - cal_from_sat_tran_fat (str)  
        - calories_per_serving (str)    
        - carbo_per_serving (str)    
        - carbo_uom (str)    
        - cholesterol_per_serving (str)    
        - cholesterol_uom (str)    
        - dvp_biotin (str)    
        - dvp_calcium (str)    
        - dvp_carbo (str)    
        - dvp_chloride (str)    
        - dvp_cholesterol (str)    
        - dvp_chromium (str)
        - dvp_copper (str)
        - dvp_fiber (str)
        - dvp_folic_acid (str)
        - dvp_iodide (str)
        - dvp_iodine (str)
        - dvp_iron (str)
        - dvp_magnesium (str)
        - dvp_manganese (str)
        - dvp_molybdenum (str)
        - dvp_niacin (str)
        - dvp_panthothenate (str)
        - dvp_phosphorus (str)
        - dvp_potassium (str)
        - dvp_protein (str)
        - dvp_riboflavin (str)
        - dvp_sat_tran_fat (str)
        - dvp_saturated_fat (str)
        - dvp_selenium (str)
        - dvp_sodium (str)
        - dvp_sugar (str)
        - dvp_thiamin (str)
        - dvp_total_fat (str)
        - dvp_vitamin_a (str)
        - dvp_vitamin_b12 (str)
        - dvp_vitamin_b6 (str)
        - dvp_vitamin_c (str)
        - dvp_vitamin_d (str)
        - dvp_vitamin_e (str)
        - dvp_vitamin_k (str)
        - dvp_zinc (str)
        - fat_calories_per_serving (str)
        - fiber_per_serving (str)
        - fiber_uom (str)
        - insol_fiber_per_serving (str)
        - insol_fiber_per_serving_uom (str)
        - mono_unsat_fat (str)
        - mono_unsat_fat_uom (str)
        - nutrition_label (str)
        - omega_3_polyunsat (str)
        - omega_3_polyunsat_uom (str)
        - omega_6_polyunsat (str)
        - omega_6_polyunsat_uom (str)
        - omega_9_polyunsat (str)
        - omega_9_polyunsat_uom (str)
        - poly_unsat_fat (str)
        - poly_unsat_fat_uom (str)
        - potassium_per_serving (str)
        - potassium_uom (str)
        - protein_per_serving (str)
        - protein_uom (str)
        - sat_fat_per_serving (str)
        - sat_fat_uom (str)
        - serving_size (str)
        - serving_size_uom (str)
        - servings_per_container (str)
        - sodium_per_serving (str)
        - sodium_uom (str)
        - sol_fiber_per_serving (str)
        - sol_fiber_per_serving_uom (str)
        - starch_per_serving (str)
        - starch_per_serving_uom (str)
        - sub_number (int)
        - sugar_per_serving (str)
        - sugar_uom (str)
        - suger_alc_per_serving (str)
        - suger_alc_per_serving_uom (str)
        - total_fat_per_serving (str)
        - total_fat_uom (str)
        - trans_fat_per_serving (str)
        - trans_fat_uom (str)
        
    Client-specific properties, not intended for general use.
        - nutrient_disclaimer_1 (str)
        - nutrient_disclaimer_2 (str)
        - nutrient_disclaimer_3 (str)
        - nutrient_disclaimer_4 (str)

    """

    elements_properties = OrderedDict([
        ('cal_from_sat_tran_fat', ('cal_from_sat_tran_fat', str)),
        ('calories_per_serving', ('calories_per_serving', str)),
        ('carbo_per_serving', ('carbo_per_serving', str)),
        ('carbo_uom', ('carbo_uom', str)),
        ('cholesterol_per_serving', ('cholesterol_per_serving', str)),
        ('cholesterol_uom', ('cholesterol_uom', str)),
        ('dvp_biotin', ('dvp_biotin', str)),
        ('dvp_calcium', ('dvp_calcium', str)),
        ('dvp_carbo', ('dvp_carbo', str)),
        ('dvp_chloride', ('dvp_chloride', str)),
        ('dvp_cholesterol', ('dvp_cholesterol', str)),
        ('dvp_chromium', ('dvp_chromium', str)),
        ('dvp_copper', ('dvp_copper', str)),
        ('dvp_fiber', ('dvp_fiber', str)),
        ('dvp_folic_acid', ('dvp_folic_acid', str)),
        ('dvp_iodide', ('dvp_iodide', str)),
        ('dvp_iodine', ('dvp_iodine', str)),
        ('dvp_iron', ('dvp_iron', str)),
        ('dvp_magnesium', ('dvp_magnesium', str)),
        ('dvp_manganese', ('dvp_manganese', str)),
        ('dvp_molybdenum', ('dvp_molybdenum', str)),
        ('dvp_niacin', ('dvp_niacin', str)),
        ('dvp_panthothenate', ('dvp_panthothenate', str)),
        ('dvp_phosphorus', ('dvp_phosphorus', str)),
        ('dvp_potassium', ('dvp_potassium', str)),
        ('dvp_protein', ('dvp_protein', str)),
        ('dvp_riboflavin', ('dvp_riboflavin', str)),
        ('dvp_sat_tran_fat', ('dvp_sat_tran_fat', str)),
        ('dvp_saturated_fat', ('dvp_saturated_fat', str)),
        ('dvp_selenium', ('dvp_selenium', str)),
        ('dvp_sodium', ('dvp_sodium', str)),
        ('dvp_sugar', ('dvp_sugar', str)),
        ('dvp_thiamin', ('dvp_thiamin', str)),
        ('dvp_total_fat', ('dvp_total_fat', str)),
        ('dvp_vitamin_a', ('dvp_vitamin_a', str)),
        ('dvp_vitamin_b12', ('dvp_vitamin_b12', str)),
        ('dvp_vitamin_b6', ('dvp_vitamin_b6', str)),
        ('dvp_vitamin_c', ('dvp_vitamin_c', str)),
        ('dvp_vitamin_d', ('dvp_vitamin_d', str)),
        ('dvp_vitamin_e', ('dvp_vitamin_e', str)),
        ('dvp_vitamin_k', ('dvp_vitamin_k', str)),
        ('dvp_zinc', ('dvp_zinc', str)),
        ('fat_calories_per_serving', ('fat_calories_per_serving', str)),
        ('fiber_per_serving', ('fiber_per_serving', str)),
        ('fiber_uom', ('fiber_uom', str)),
        ('insol_fiber_per_serving', ('insol_fiber_per_serving', str)),
        ('insol_fiber_per_serving_uom', ('insol_fiber_per_serving_uom', str)),
        ('mono_unsat_fat', ('mono_unsat_fat', str)),
        ('mono_unsat_fat_uom', ('mono_unsat_fat_uom', str)),
        ('nutrient_disclaimer_1', ('nutrient_disclaimer_1', str)),
        ('nutrient_disclaimer_2', ('nutrient_disclaimer_2', str)),
        ('nutrient_disclaimer_3', ('nutrient_disclaimer_3', str)),
        ('nutrient_disclaimer_4', ('nutrient_disclaimer_4', str)),
        ('nutrition_label', ('nutrition_label', str)),
        ('omega_3_polyunsat', ('omega_3_polyunsat', str)),
        ('omega_3_polyunsat_uom', ('omega_3_polyunsat_uom', str)),
        ('omega_6_polyunsat', ('omega_6_polyunsat', str)),
        ('omega_6_polyunsat_uom', ('omega_6_polyunsat_uom', str)),
        ('omega_9_polyunsat', ('omega_9_polyunsat', str)),
        ('omega_9_polyunsat_uom', ('omega_9_polyunsat_uom', str)),
        ('poly_unsat_fat', ('poly_unsat_fat', str)),
        ('poly_unsat_fat_uom', ('poly_unsat_fat_uom', str)),
        ('potassium_per_serving', ('potassium_per_serving', str)),
        ('potassium_uom', ('potassium_uom', str)),
        ('protein_per_serving', ('protein_per_serving', str)),
        ('protein_uom', ('protein_uom', str)),
        ('sat_fat_per_serving', ('sat_fat_per_serving', str)),
        ('sat_fat_uom', ('sat_fat_uom', str)),
        ('serving_size', ('serving_size', str)),
        ('serving_size_uom', ('serving_size_uom', str)),
        ('servings_per_container', ('servings_per_container', str)),
        ('sodium_per_serving', ('sodium_per_serving', str)),
        ('sodium_uom', ('sodium_uom', str)),
        ('sol_fiber_per_serving', ('sol_fiber_per_serving', str)),
        ('sol_fiber_per_serving_uom', ('sol_fiber_per_serving_uom', str)),
        ('starch_per_serving', ('starch_per_serving', str)),
        ('starch_per_serving_uom', ('starch_per_serving_uom', str)),
        ('sub_number', ('sub_number', int)),
        ('sugar_per_serving', ('sugar_per_serving', str)),
        ('sugar_uom', ('sugar_uom', str)),
        ('suger_alc_per_serving', ('sugar_alc_per_serving', str)),
        ('suger_alc_per_serving_uom', ('sugar_alc_per_serving_uom', str)),
        ('total_fat_per_serving', ('total_fat_per_serving', str)),
        ('total_fat_uom', ('total_fat_uom', str)),
        ('trans_fat_per_serving', ('trans_fat_per_serving', str)),
        ('trans_fat_uom', ('trans_fat_uom', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='kwikee_nutrition',  # type: str
    ):
        self.cal_from_sat_tran_fat = None  # type: Optional[str]
        self.calories_per_serving = None  # type: Optional[str]
        self.carbo_per_serving = None  # type: Optional[str]
        self.carbo_uom = None  # type: Optional[str]
        self.cholesterol_per_serving = None  # type: Optional[str]
        self.cholesterol_uom = None  # type: Optional[str]
        self.dvp_biotin = None  # type: Optional[str]
        self.dvp_calcium = None  # type: Optional[str]
        self.dvp_carbo = None  # type: Optional[str]
        self.dvp_chloride = None  # type: Optional[str]
        self.dvp_cholesterol = None  # type: Optional[str]
        self.dvp_chromium = None  # type: Optional[str]
        self.dvp_copper = None  # type: Optional[str]
        self.dvp_fiber = None  # type: Optional[str]
        self.dvp_folic_acid = None  # type: Optional[str]
        self.dvp_iodide = None  # type: Optional[str]
        self.dvp_iodine = None  # type: Optional[str]
        self.dvp_iron = None  # type: Optional[str]
        self.dvp_magnesium = None  # type: Optional[str]
        self.dvp_manganese = None  # type: Optional[str]
        self.dvp_molybdenum = None  # type: Optional[str]
        self.dvp_niacin = None  # type: Optional[str]
        self.dvp_panthothenate = None  # type: Optional[str]
        self.dvp_phosphorus = None  # type: Optional[str]
        self.dvp_potassium = None  # type: Optional[str]
        self.dvp_protein = None  # type: Optional[str]
        self.dvp_riboflavin = None  # type: Optional[str]
        self.dvp_sat_tran_fat = None  # type: Optional[str]
        self.dvp_saturated_fat = None  # type: Optional[str]
        self.dvp_selenium = None  # type: Optional[str]
        self.dvp_sodium = None  # type: Optional[str]
        self.dvp_sugar = None  # type: Optional[str]
        self.dvp_thiamin = None  # type: Optional[str]
        self.dvp_total_fat = None  # type: Optional[str]
        self.dvp_vitamin_a = None  # type: Optional[str]
        self.dvp_vitamin_b12 = None  # type: Optional[str]
        self.dvp_vitamin_b6 = None  # type: Optional[str]
        self.dvp_vitamin_c = None  # type: Optional[str]
        self.dvp_vitamin_d = None  # type: Optional[str]
        self.dvp_vitamin_e = None  # type: Optional[str]
        self.dvp_vitamin_k = None  # type: Optional[str]
        self.dvp_zinc = None  # type: Optional[str]
        self.fat_calories_per_serving = None  # type: Optional[str]
        self.fiber_per_serving = None  # type: Optional[str]
        self.fiber_uom = None  # type: Optional[str]
        self.insol_fiber_per_serving = None  # type: Optional[str]
        self.insol_fiber_per_serving_uom = None  # type: Optional[str]
        self.mono_unsat_fat = None  # type: Optional[str]
        self.mono_unsat_fat_uom = None  # type: Optional[str]
        self.nutrient_disclaimer_1 = None  # type: Optional[str]
        self.nutrient_disclaimer_2 = None  # type: Optional[str]
        self.nutrient_disclaimer_3 = None  # type: Optional[str]
        self.nutrient_disclaimer_4 = None  # type: Optional[str]
        self.nutrition_label = None  # type: Optional[str]
        self.omega_3_polyunsat = None  # type: Optional[str]
        self.omega_3_polyunsat_uom = None  # type: Optional[str]
        self.omega_6_polyunsat = None  # type: Optional[str]
        self.omega_6_polyunsat_uom = None  # type: Optional[str]
        self.omega_9_polyunsat = None  # type: Optional[str]
        self.omega_9_polyunsat_uom = None  # type: Optional[str]
        self.poly_unsat_fat = None  # type: Optional[str]
        self.poly_unsat_fat_uom = None  # type: Optional[str]
        self.potassium_per_serving = None  # type: Optional[str]
        self.potassium_uom = None  # type: Optional[str]
        self.protein_per_serving = None  # type: Optional[str]
        self.protein_uom = None  # type: Optional[str]
        self.sat_fat_per_serving = None  # type: Optional[str]
        self.sat_fat_uom = None  # type: Optional[str]
        self.serving_size = None  # type: Optional[str]
        self.serving_size_uom = None  # type: Optional[str]
        self.servings_per_container = None  # type: Optional[str]
        self.sodium_per_serving = None  # type: Optional[str]
        self.sodium_uom = None  # type: Optional[str]
        self.sol_fiber_per_serving = None  # type: Optional[str]
        self.sol_fiber_per_serving_uom = None  # type: Optional[str]
        self.starch_per_serving = None  # type: Optional[str]
        self.starch_per_serving_uom = None  # type: Optional[str]
        self.sub_number = None  # type: Optional[int]
        self.sugar_per_serving = None  # type: Optional[str]
        self.sugar_uom = None  # type: Optional[str]
        self.sugar_alc_per_serving = None  # type: Optional[str]
        self.sugar_alc_per_serving_uom = None  # type: Optional[str]
        self.total_fat_per_serving = None  # type: Optional[str]
        self.total_fat_uom = None  # type: Optional[str]
        self.trans_fat_per_serving = None  # type: Optional[str]
        self.trans_fat_uom = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Nutrition(SOAPElement):
    """
    Properties:
    
        - kwikee_nutrition (Sequence[KwikeeNutrition]): A sequence of `KwikeeNutrition` objects containing information 
          found on the "Nutrition Facts" panel of a product's packaging.
    """

    elements_properties = OrderedDict([
        ('kwikee_nutrition', ('kwikee_nutrition', (KwikeeNutrition,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='nutrition',  # type: str
    ):
        self.kwikee_nutrition = []  # type: Sequence[KwikeeNutrition]
        super().__init__(xml=xml, tag=tag)


class RomanceCopy(SOAPElement):
    """
    Properties:
    
        - kwikee_romance (Sequence[str]): Detailed information that describes a product or markets the product and/or 
          company to consumers. Typically the number of instances is ~3.

    """

    elements_properties = OrderedDict([
        ('kwikee_romance', ('kwikee_romance', (str,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='romance_copy',  # type: str
    ):
        self.kwikee_romance = []  # type: Sequence[str]
        super().__init__(xml=xml, tag=tag)


class ComponentElement(SOAPElement):
    """
    Properties:
    
        - format (str)        
        - name (str)        
        - value (Sequence[str])
    """

    elements_properties = OrderedDict([
        ('format', ('format', str)),
        ('name', ('name', str)),
        ('values/value', ('values', (str,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='component_element',  # type: str
    ):
        self.format = None  # type: Optional[str]
        self.name = None  # type: Optional[str]
        self.values = []  # type: Sequence[str]
        super().__init__(xml=xml, tag=tag)


class Segment(SOAPElement):
    """
    Properties:
    
        - column_heading (Sequence[str])        
        - component_element (Sequence[ComponentElement])
    """

    elements_properties = OrderedDict([
        ('column_headings/column_heading', ('column_headings', (str,))),
        ('component_elements/component_element', ('component_elements', (ComponentElement,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='segment',  # type: str
    ):
        self.column_headings = []  # type: Sequence[str]
        self.component_elements = []  # type: Sequence[ComponentElement]
        super().__init__(xml=xml, tag=tag)


class SupplementalFacts(SOAPElement):
    """
    Properties:
    
        - footer (Sequence[str])        
        - footnote (Sequence[str])        
        - headers (Sequence[str])        
        - segments (Sequence[Segments])
    """

    elements_properties = OrderedDict([
        ('footers/footer', ('footers', (str,))),
        ('footnotes/footnote', ('footnotes', (str,))),
        ('headers/header', ('headers', (str,))),
        ('segments/segment', ('segments', (Segment,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='supplemental_facts',  # type: str
    ):
        self.footers = []  # type: Sequence[str]
        self.footnotes = []  # type: Sequence[str]
        self.headers = []  # type: Sequence[str]
        self.segments = []  # type: Sequence[Segments]
        super().__init__(xml=xml, tag=tag)


class WhyBuyCopy(SOAPElement):
    """
    A container object for small specifics on the product packaging (usually the front) that are used to entice
    a consumer to buy the product.
    
    Properties:
    
        - kwikee_why_buy (Sequence[str]): A sequence of small specifics on the product packaging (usually the front)
          that are used to entice a consumer to buy the product.
    """

    elements_properties = OrderedDict([
        ('kwikee_why_buy', ('kwikee_why_buy', (str,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='why_buy_copy',  # type: str
    ):
        self.kwikee_why_buy = []  # type: Sequence[str]
        super().__init__(xml=xml, tag=tag)


class NutritionalClaims(SOAPElement):
    """    
    Properties:
    
        - bpa_free (bool): Is the product BPA free?
        - fat_free (bool): Is the product fat free?
        - fsc (bool): Is the product Forest Stewardship Council certified?
        - halal (bool): Is the product certified halal?
        - kosher (bool): Is the product certified kosher?
        - low_fat (bool): Is the product low fat?
        - non_food (bool): Is the product inedible?
        - non_gmo (bool): Is the product GMO free?
        - organic (bool): Is the product organic?
        - vegan (bool): Is the product vegan?
        - vegetarian (bool): Is the product vegetarian?
    """

    elements_properties = OrderedDict([
        ('bpa_free', ('bpa_free', bool)),
        ('fat_free', ('fat_free', bool)),
        ('fsc', ('fsc', bool)),
        ('halal', ('halal', bool)),
        ('kosher', ('kosher', bool)),
        ('low_fat', ('low_fat', bool)),
        ('non_food', ('non_food', bool)),
        ('non_gmo', ('non_gmo', bool)),
        ('organic', ('organic', bool)),
        ('vegan', ('vegan', bool)),
        ('vegetarian', ('vegetarian', bool)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='nutritional_claims',  # type: str
    ):
        self.bpa_free = None  # type: Optional[bool]
        self.fat_free = None  # type: Optional[bool]
        self.fsc = None  # type: Optional[bool]
        self.halal = None  # type: Optional[bool]
        self.kosher = None  # type: Optional[bool]
        self.low_fat = None  # type: Optional[bool]
        self.non_food = None  # type: Optional[bool]
        self.non_gmo = None  # type: Optional[bool]
        self.organic = None  # type: Optional[bool]
        self.vegan = None  # type: Optional[bool]
        self.vegetarian = None  # type: Optional[bool]
        super().__init__(xml=xml, tag=tag)


class Profile(SOAPElement):
    """    
    Properties:
    
        - action (str): The type of action to perform with the data in this profile. Valid values:
            + "UPDATE"
            + "INSERT"
            + "DELETE"
            + "NO CHANGE"
        - allergens (str): Allergen warnings.
        - brand (Brand): A container object for the product's brand identification and name.
        - case_gtin (str): A 14-digit Global Trade Item Number (GTIN-14) associated with this product's "case" logistics 
          unit.
        - case_count (str): The number of consumer units contained in a case of this product.
        - case_height (str): The height of this product's "case" logistics unit, expressed in inches.
        - case_width (str): The width of this product's "case" logistics unit, expressed in inches.
        - case_depth (str): The depth (length measured from front to back) of this product's "case" logistics unit, 
          expressed in inches.
        - category (str): A Kwikee shelf management categorization.
        - consumable (str): Can I eat this?
        - container_type (str): The type of container in which this product is sold to the consumer. Possible values 
          include, but are not limited to: "AEROSOL CAN", "BAG", "BLISTER", "BOTTLE", "BOX", "CAN", "CUP", 
          "GLASS BOTTLE", "JUG", "PACK", "PEG", "PLASTIC BOTTLE", "POUCH", "PUMP", "SLEEVE", "SQUEEZE BOTTLE",
          "STICK", "TUB", "TUBE", and "WRAPPER".
        - custom_product_name (str): A single statement that describes the product as completely as possible for the 
          consumer. This often includes the brand name, product name, size and the container type. Example: "Kraft 
          Dinners Deluxe Original Cheddar Macaroni & Cheese Dinner 14 oz. Box".
        - department (str): A Kwikee shelf management categorization.
        - depth (Decimal): The depth (length measured from front to back) of the (packaged) consumer unit of this 
          product, expressed in inches.
        - height (Decimal): The height of the (packaged) consumer unit of this item, expressed in inches.
        - width (Decimal): The width of the (packaged) consumer unit of this item, expressed in inches.
        - description (str): A brief description of the product.        
        - display_depth (str): The depth of the merchandising display for this product.
        - display_height (str): The height of the merchandising display for this product.
        - display_width (str): The width of the merchandising display for this product.
        - division_name (str): A manufacturer-specific categorization.
        - division_name_2 (str): An additional manufacturer-specific categorization.
        - extra_text_2 (str)
        - extra_text_3 (str)
        - extra_text_4 (str)
        - fat_free (str): Flag indicating a claim that a food is fat-free, or any claim likely to have the same meaning
          for the consumer. May only be made where the product contains no more than 0.5g of fat per 100g or 100ml.
        - gluten_free (bool): Flag indicating that the product claims to be gluten free.
        - gpc_attributes_assigned (datetime) 
        - gpc_brick_id (str): Brick is the lowest-level of Global Product Classification (GPC). The brick ID is an 
          8-digit string composed of integers, representing a unique ID for the classification. For more information, 
          see: http://www.gs1.org/gpc.
        - gpc_brick_name (str): Brick is the lowest-level of Global Product Classification (GPC). For more information, 
          see: http://www.gs1.org/gpc.
        - gpc_class_id (str): Class is the second-lowest-level of Global Product Classification (GPC). The class ID is 
          an 8-digit string composed of integers, representing a unique ID for the classification. For more information,
          see: http://www.gs1.org/gpc.
        - gpc_class_name (str): Class is the second-lowest-level of Global Product Classification (GPC). For more 
          information, see: http://www.gs1.org/gpc.
        - gpc_family_id (str): Family is the third-lowest-level of Global Product Classification (GPC). The family ID is
          an 8-digit string composed of integers, representing a unique ID for the classification. For more information,
          see: http://www.gs1.org/gpc.
        - gpc_family_name (str): Family is the third-lowest-level of Global Product Classification (GPC). For more 
          information, see: http://www.gs1.org/gpc.
        - gpc_segment_id (str): Segment is the highest level of Global Product Classification (GPC). The segment ID is
          an 8-digit string composed of integers, representing a unique ID for the classification. For more information,
          see: http://www.gs1.org/gpc.
        - gpc_segment_name (str): Segment is the highest level of Global Product Classification (GPC). For more 
          information, see: http://www.gs1.org/gpc.
        - guarantee_analysis (str): A list giving the quantities of various nutrients in a pet food. This is the 
          equivalent of a nutrition panel for pet foods.
        - guarantees (str): Promises of satisfaction, compensation or replacement included on a product, such as a 
          warranty or a money-back guarantee.
        - indications_copy (str): Information labeled "indications" on drugs.
        - ingredients (str): The listed ingredients on foods and beverages, pet foods, or beauty products, the active 
          and inactive ingredients on drugs, or the components of chemicals.
        - instructions (Sequence[str]): A sequence of various types of instruction from the product
          packaging——potentially including preparation instructions, storage instructions, precautions, or 
          safety information. 
        - interactions_copy (str): Information labeled "interactions" on drugs.
        - kosher (str): Is this product certified as being Kosher?
        - legal (str): Contact information for the manufacturer or distributor, information about the country of 
          manufacture, and any legal disclaimers on the package.
        - low_fat (str): Flag indicating a claim that a food is low in fat, or any claim likely to have the same meaning
          for the consumer. May only be made where the product contains no more than 3g of fat per 100g or 1.5g of fat 
          per 100ml.
        - manufacturer (Manufacturer): The name and ID of the product's manufacturer.
        - mfr_approved_date (datetime): The date when the manufacturer last approved their image and data assets.
        - model (str): The product's model number.
        - multiple_shelf_facings (bool): A flag indicating that a product has two or more equaly large advertising 
          surfaces with different orientations.
        - nutrition (Nutrition): Information found on the "Nutrition Facts" panel of a product's packaging. Each profile 
          record can have 1 or more nutrition records, however the most common number of nutrition records is 2——one for 
          base nutrition, and one for “As Prepared”.
        - organic (bool): A flag indicating whether the product is certified Organic.
        - peg_down (Decimal): The measurement from the center of the peg hole to the top of the packaging.
        - peg_right (Decimal): The measurement from the center of the peg hole to the left side of the packaging.
        - physical_weight_lb (Decimal): The net weight of the product in pounds.
        - physical_weight_oz (Decimal): The net weight of the product in ounces.
        - post_consumer (str): Information related to the disposal of the product packaging, specifically whether or not
          the packaging is recyclable, biodegradable, or made from recycled materials.
        - product_count (str): If the product is a multipack or contains multiple distinct elements, such as a 6-pack of
          soda or a box of 10 fruit snack pouches, this is the number of elements (6 and 10, respectively). 
        - product_name (str): Product_name answers the question "What is the product?" in one or two words. The simplest
          descriptor of what the product is.
        - product_size (Decimal): The weight, volume, or count that describes the size of the product in concert with 
          the `uom` property which indicates the corresponding unit-of-measure. For example, if the product were a 16 
          oz. bag of cereal, the `product_size` would be "16".
        - profile_id (int): A unique identifier for this profile.
        - romance_copy (RomanceCopy): A container object for detailed information that describes a product or markets
          the product and/or company to consumers.
        - section_id (str): A Kwikee shelf management categorization.
        - section_name (str): A Kwikee shelf management categorization.
        - supplemental_facts (SupplementalFacts): Additional information found on the "Supplement Facts" or 
          "Nutrition Facts" panel of a product's packaging. Most products do not have supplemental facts.
        - tray_count (str): Number of items in a tray of the product.
        - tray_height (str): The height of the tray used to merchandise this product.
        - tray_width (str): The width of the tray used to merchandise this product.
        - unit_container (str): Any internal container(s) which directly hold the product. For example, if the product
          were a six-pack of 12 fl. oz. cans of Coke, the Container Type would be CAN.
        - unit_size (Decimal): The weight, volume, or count that describes the size of the all individually packaged 
          units within the full product. For example, if the product were a six-pack of 12 fl. oz. cans of Coke, the 
          Unit Size would be 12.
        - unit_uom (str): The unit or measurement attached to the Unit Size field. For example, if the product were a 
          six-pack of 12 fl. oz. cans of Coke, the Unit UOM would be "FL OZ".
        - uom (str): The unit or measurement attached to the product_size field. For example, if the product were a
          16 oz. bag of cereal, the uom would be "OZ".
        - warnings_copy (str): Warning or cautionary statements on the package, often preceded by the words "Warning," 
          "Caution", "Hazardous", or "Important".
        - why_buy_copy (WhyBuyCopy): Small specifics on the product packaging (usually the front) that are used to 
          entice a consumer to buy the product.
        - nutritional_claims (NutritionalClaims): A container object for various nutritional claims found on the 
          product's packaging.
        - vendor_marketing_bullets (Sequence[str])
        - vendor_marketing_statements (Sequence[str])
          
    Properties for Kwikee internal-use only:
    
        - image_indicator (int): For Kwikee internal use only.
        - override_manufacturer_tasks (bool)
        - last_publish_date (datetime)
        - promotion (str)
        
    Client-specific properties, not intended for general use:
    
        - nutrient_claim_1 (str)
        - nutrient_claim_2 (str)
        - nutrient_claim_3 (str)
        - nutrient_claim_4 (str)
        - nutrient_claim_5 (str)
        - nutrient_claim_6 (str)
        - nutrient_claim_7 (str)
        - nutrient_claim_8 (str)
        - ingredient_code (str)
        - is_variant_flag (bool)
        - variant_name_1 (str)
        - variant_name_2 (str)
        - variant_value_1 (str)
        - variant_value_2 (str)
        - vm_claim_1 (str)
        - vm_claim_2 (str)
        - vm_claim_3 (str)
        - vm_claim_4 (str)
        - vm_type_1 (str)
        - vm_type_2 (str)
        - vm_type_3 (str)
        - vm_type_4 (str)
        - nutrition_footnotes_1 (str)
        - nutrition_footnotes_2 (str)
        - nutrition_head_1 (str)
        - nutrition_head_2 (str)
        - other_nutrient_statement (str)
        - diabetes_fc_values (str)
        - disease_claim (str)
        - product_form (str)
        - product_type (str)
        - sensible_solutions (str)
        - size_description_1 (str)
        - size_description_2 (str)
        - ss_claim_1 (str)
        - ss_claim_2 (str)
        - ss_claim_3 (str)
        - ss_claim_4 (str)
        - romance_copy_category (str)
    """

    elements_properties = OrderedDict([
        ('action', ('action', str)),
        ('case_gtin', ('case_gtin', str)),
        ('brand', ('brand', Brand)),
        ('case_count', ('case_count', str)),
        ('case_width', ('case_width', str)),
        ('case_height', ('case_height', str)),
        ('case_depth', ('case_depth', str)),
        ('allergens', ('allergens', str)),
        ('category', ('category', str)),
        ('consumable', ('consumable', str)),
        ('container_type', ('container_type', str)),
        ('custom_product_name', ('custom_product_name', str)),
        ('department', ('department', str)),
        ('depth', ('depth', Decimal)),
        ('height', ('height', Decimal)),
        ('width', ('width', Decimal)),
        ('description', ('description', str)),
        ('diabetes_fc_values', ('diabetes_fc_values', str)),
        ('disease_claim', ('disease_claim', str)),
        ('display_depth', ('display_depth', str)),
        ('display_height', ('display_height', str)),
        ('display_width', ('display_width', str)),
        ('division_name', ('division_name', str)),
        ('division_name_2', ('division_name_2', str)),
        ('extra_text_2', ('extra_text_2', str)),
        ('extra_text_3', ('extra_text_3', str)),
        ('extra_text_4', ('extra_text_4', str)),
        ('fat_free', ('fat_free', str)),
        ('gluten_free', ('gluten_free', bool)),
        ('gpc_attributes_assigned', ('gpc_attributes_assigned', datetime)),
        ('gpc_brick_id', ('gpc_brick_id', str)),
        ('gpc_brick_name', ('gpc_brick_name', str)),
        ('gpc_class_id', ('gpc_class_id', str)),
        ('gpc_class_name', ('gpc_class_name', str)),
        ('gpc_family_id', ('gpc_family_id', str)),
        ('gpc_family_name', ('gpc_family_name', str)),
        ('gpc_segment_id', ('gpc_segment_id', str)),
        ('gpc_segment_name', ('gpc_segment_name', str)),
        ('guarantee_analysis', ('guarantee_analysis', str)),
        ('guarantees', ('guarantees', str)),
        ('image_indicator', ('image_indicator', int)),
        ('indications_copy', ('indications_copy', str)),
        ('ingredient_code', ('ingredient_code', str)),
        ('ingredients', ('ingredients', str)),
        ('instructions/kwikee_instruction', ('instructions', (str,))),
        ('interactions_copy', ('interactions_copy', str)),
        ('is_variant_flag', ('is_variant_flag', bool)),
        ('kosher', ('kosher', str)),
        ('last_publish_date', ('last_publish_date', datetime)),
        ('legal', ('legal', str)),
        ('low_fat', ('low_fat', str)),
        ('manufacturer', ('manufacturer', Manufacturer)),
        ('mfr_approved_date', ('mfr_approved_date', datetime)),
        ('model', ('model', str)),
        ('multiple_shelf_facings', ('multiple_shelf_facings', bool)),
        ('nutrient_claim_1', ('nutrient_claim_1', str)),
        ('nutrient_claim_2', ('nutrient_claim_2', str)),
        ('nutrient_claim_3', ('nutrient_claim_3', str)),
        ('nutrient_claim_4', ('nutrient_claim_4', str)),
        ('nutrient_claim_5', ('nutrient_claim_5', str)),
        ('nutrient_claim_6', ('nutrient_claim_6', str)),
        ('nutrient_claim_7', ('nutrient_claim_7', str)),
        ('nutrient_claim_8', ('nutrient_claim_8', str)),
        ('nutrition', ('nutrition', Nutrition)),
        ('nutrition_footnotes_1', ('nutrition_footnotes_1', str)),
        ('nutrition_footnotes_2', ('nutrition_footnotes_2', str)),
        ('nutrition_head_1', ('nutrition_head_1', str)),
        ('nutrition_head_2', ('nutrition_head_2', str)),
        ('nutritional_claims', ('nutritional_claims', NutritionalClaims)),
        ('organic', ('organic', bool)),
        ('other_nutrient_statement', ('other_nutrient_statement', str)),
        ('override_manufacturer_tasks', ('override_manufacturer_tasks', bool)),
        ('peg_down', ('peg_down', Decimal)),
        ('peg_right', ('peg_right', Decimal)),
        ('physical_weight_lb', ('physical_weight_lb', Decimal)),
        ('physical_weight_oz', ('physical_weight_oz', Decimal)),
        ('post_consumer', ('post_consumer', str)),
        ('product_count', ('product_count', str)),
        ('product_form', ('product_form', str)),
        ('product_name', ('product_name', str)),
        ('product_size', ('product_size', Decimal)),
        ('product_type', ('product_type', str)),
        ('profile_id', ('profile_id', int)),
        ('promotion', ('promotion', str)),
        ('romance_copy', ('romance_copy', RomanceCopy)),
        ('romance_copy_category', ('romance_copy_category', str)),
        ('section_id', ('section_id', str)),
        ('section_name', ('section_name', str)),
        ('sensible_solutions', ('sensible_solutions', str)),
        ('size_description_1', ('size_description_1', str)),
        ('size_description_2', ('size_description_2', str)),
        ('ss_claim_1', ('ss_claim_1', str)),
        ('ss_claim_2', ('ss_claim_2', str)),
        ('ss_claim_3', ('ss_claim_3', str)),
        ('ss_claim_4', ('ss_claim_4', str)),
        ('supplemental_facts', ('supplemental_facts', SupplementalFacts)),
        ('tray_count', ('tray_count', str)),
        ('tray_height', ('tray_height', str)),
        ('tray_width', ('tray_width', str)),
        ('unit_container', ('unit_container', str)),
        ('unit_size', ('unit_size', Decimal)),
        ('unit_uom', ('unit_uom', str)),
        ('uom', ('uom', str)),
        ('variant_name_1', ('variant_name_1', str)),
        ('variant_name_2', ('variant_name_2', str)),
        ('variant_value_1', ('variant_value_1', str)),
        ('variant_value_2', ('variant_value_2', str)),
        ('vendor_marketing_bullets/vendor_marketing_bullet', ('vendor_marketing_bullets', (str,))),
        ('vendor_marketing_statements/vendor_marketing_statement', ('vendor_marketing_statements', (str,))),
        ('vm_claim_1', ('vm_claim_1', str)),
        ('vm_claim_2', ('vm_claim_2', str)),
        ('vm_claim_3', ('vm_claim_3', str)),
        ('vm_claim_4', ('vm_claim_4', str)),
        ('vm_type_1', ('vm_type_1', str)),
        ('vm_type_2', ('vm_type_2', str)),
        ('vm_type_3', ('vm_type_3', str)),
        ('vm_type_4', ('vm_type_4', str)),
        ('warnings_copy', ('warnings_copy', str)),
        ('why_buy_copy', ('why_buy_copy', WhyBuyCopy))
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='profile',  # type: str
    ):
        self.action = None  # type: Optional[str]
        self.case_gtin = None  # type: Optional[str]
        self.brand = None  # type: Optional[Brand]
        self.case_count = None  # type: Optional[str]
        self.case_width = None  # type: Optional[str]
        self.case_height = None  # type: Optional[str]
        self.case_depth = None  # type: Optional[str]
        self.allergens = None  # type: Optional[str]
        self.category = None  # type: Optional[str]
        self.consumable = None  # type: Optional[str]
        self.container_type = None  # type: Optional[str]
        self.custom_product_name = None  # type: Optional[str]
        self.department = None  # type: Optional[str]
        self.depth = None  # type: Optional[Decimal]
        self.height = None  # type: Optional[Decimal]
        self.width = None  # type: Optional[Decimal]
        self.description = None  # type: Optional[str]
        self.diabetes_fc_values = None  # type: Optional[str]
        self.disease_claim = None  # type: Optional[str]
        self.display_depth = None  # type: Optional[str]
        self.display_height = None  # type: Optional[str]
        self.display_width = None  # type: Optional[str]
        self.division_name = None  # type: Optional[str]
        self.division_name_2 = None  # type: Optional[str]
        self.extra_text_2 = None  # type: Optional[str]
        self.extra_text_3 = None  # type: Optional[str]
        self.extra_text_4 = None  # type: Optional[str]
        self.fat_free = None  # type: Optional[str]
        self.gluten_free = None  # type: Optional[bool]
        self.gpc_attributes_assigned = None  # type: Optional[datetime]
        self.gpc_brick_id = None  # type: Optional[str]
        self.gpc_brick_name = None  # type: Optional[str]
        self.gpc_class_id = None  # type: Optional[str]
        self.gpc_class_name = None  # type: Optional[str]
        self.gpc_family_id = None  # type: Optional[str]
        self.gpc_family_name = None  # type: Optional[str]
        self.gpc_segment_id = None  # type: Optional[str]
        self.gpc_segment_name = None  # type: Optional[str]
        self.guarantee_analysis = None  # type: Optional[str]
        self.guarantees = None  # type: Optional[str]
        self.image_indicator = None  # type: Optional[int]
        self.indications_copy = None  # type: Optional[str]
        self.ingredient_code = None  # type: Optional[str]
        self.ingredients = None  # type: Optional[str]
        self.instructions = []  # type: Sequence[str]
        self.interactions_copy = None  # type: Optional[str]
        self.is_variant_flag = None  # type: Optional[bool]
        self.kosher = None  # type: Optional[str]
        self.last_publish_date = None  # type: Optional[datetime]
        self.legal = None  # type: Optional[str]
        self.low_fat = None  # type: Optional[str]
        self.manufacturer = None  # type: Optional[Manufacturer]
        self.mfr_approved_date = None  # type: Optional[datetime]
        self.model = None  # type: Optional[str]
        self.multiple_shelf_facings = None  # type: Optional[bool]
        self.nutrient_claim_1 = None  # type: Optional[str]
        self.nutrient_claim_2 = None  # type: Optional[str]
        self.nutrient_claim_3 = None  # type: Optional[str]
        self.nutrient_claim_4 = None  # type: Optional[str]
        self.nutrient_claim_5 = None  # type: Optional[str]
        self.nutrient_claim_6 = None  # type: Optional[str]
        self.nutrient_claim_7 = None  # type: Optional[str]
        self.nutrient_claim_8 = None  # type: Optional[str]
        self.nutrition = None  # type: Optional[Nutrition]
        self.nutrition_footnotes_1 = None  # type: Optional[str]
        self.nutrition_footnotes_2 = None  # type: Optional[str]
        self.nutrition_head_1 = None  # type: Optional[str]
        self.nutrition_head_2 = None  # type: Optional[str]
        self.organic = None  # type: Optional[bool]
        self.other_nutrient_statement = None  # type: Optional[str]
        self.override_manufacturer_tasks = None  # type: Optional[bool]
        self.peg_down = None  # type: Optional[Decimal]
        self.peg_right = None  # type: Optional[Decimal]
        self.physical_weight_lb = None  # type: Optional[Decimal]
        self.physical_weight_oz = None  # type: Optional[Decimal]
        self.post_consumer = None  # type: Optional[str]
        self.product_count = None  # type: Optional[str]
        self.product_form = None  # type: Optional[str]
        self.product_name = None  # type: Optional[str]
        self.product_size = None  # type: Optional[Decimal]
        self.product_type = None  # type: Optional[str]
        self.profile_id = None  # type: Optional[int]
        self.promotion = None  # type: Optional[str]
        self.romance_copy = None  # type: Optional[RomanceCopy]
        self.romance_copy_category = None  # type: Optional[str]
        self.section_id = None  # type: Optional[str]
        self.section_name = None  # type: Optional[str]
        self.sensible_solutions = None  # type: Optional[str]
        self.size_description_1 = None  # type: Optional[str]
        self.size_description_2 = None  # type: Optional[str]
        self.ss_claim_1 = None  # type: Optional[str]
        self.ss_claim_2 = None  # type: Optional[str]
        self.ss_claim_3 = None  # type: Optional[str]
        self.ss_claim_4 = None  # type: Optional[str]
        self.supplemental_facts = None  # type: Optional[SupplementalFacts]
        self.tray_count = None  # type: Optional[str]
        self.tray_height = None  # type: Optional[str]
        self.tray_width = None  # type: Optional[str]
        self.unit_container = None  # type: Optional[str]
        self.unit_size = None  # type: Optional[Decimal]
        self.unit_uom = None  # type: Optional[str]
        self.uom = None  # type: Optional[str]
        self.variant_name_1 = None  # type: Optional[str]
        self.variant_name_2 = None  # type: Optional[str]
        self.variant_value_1 = None  # type: Optional[str]
        self.variant_value_2 = None  # type: Optional[str]
        self.vendor_marketing_bullets = []  # type: Sequence[str]
        self.vendor_marketing_statements = []  # type: Sequence[str]
        self.vm_claim_1 = None  # type: Optional[str]
        self.vm_claim_2 = None  # type: Optional[str]
        self.vm_claim_3 = None  # type: Optional[str]
        self.vm_claim_4 = None  # type: Optional[str]
        self.vm_type_1 = None  # type: Optional[str]
        self.vm_type_2 = None  # type: Optional[str]
        self.vm_type_3 = None  # type: Optional[str]
        self.vm_type_4 = None  # type: Optional[str]
        self.warnings_copy = None  # type: Optional[str]
        self.why_buy_copy = None  # type: Optional[WhyBuyCopy]
        self.nutritional_claims = None  # type: Optional[NutritionalClaims]
        super().__init__(xml=xml, tag=tag)


class AssetFile(SOAPElement):
    """
    Properties:
    
        - type (str): The type of file referenced (file extension/MIME sub-type), such as "GS1" (a 2400 x 2400 pixel 
          JPEG image), "JPEG", "JPG", "PNG", "GIF", "TIF", "TIFF", "POG", "TGA", "PSD", "LEPS", "ZIP", "EPS", "WM",  
          "SIT", "LZIP", or "PCX".
        - url (str): The URL from where the referenced image can be retrieved.
    """

    elements_properties = OrderedDict([
        ('type', ('type', str)),
        ('url', ('url', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='file',  # type: str
    ):
        self.type = None  # type: Optional[str]
        self.url = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class AssetFiles(SOAPElement):
    """
    Properties:
    
        - file (Sequence[AssetFile]): A sequence of AssetFile instances, each containing a file type and URL.
    """

    elements_properties = OrderedDict([
        ('file', ('file', (AssetFile,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='files',  # type: str
    ):
        self.file = []  # type: Sequence[AssetFile]
        super().__init__(xml=xml, tag=tag)


class Asset(SOAPElement):
    """
    Properties:
    
        - action (str): Valid values:        
            + "UPDATE"
            + "INSERT"
            + "DELETE"
            + "NO CHANGE"                
        - asset_id (int)        
        - files (AssetFiles): A container object holding a sequence of file URLs and type indicators such as "JPEG".    
        - type (str): Example value: "swatch", for a color/pattern swatch.        
        - version (str): Used to identify images of different variations of a product which use the GTIN. For example,
          color variations such as "red", "green", or "blue".    
        - view (str): Code that defines whether the image is color or black & white and also the angle of the image.
        
        For Kwikee internal use only:
        
        - promotion (str) 
    """

    elements_properties = OrderedDict([
        ('action', ('action', str)),
        ('asset_id', ('asset_id', int)),
        ('files', ('files', AssetFiles)),
        ('type', ('type', str)),
        ('version', ('version', str)),
        ('view', ('view', str)),
        ('promotion', ('promotion', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='asset',  # type: str
    ):
        self.action = None  # type: Optional[str]
        self.asset_id = None  # type: Optional[int]
        self.files = None  # type: Optional[AssetFiles]
        self.type = None  # type: Optional[str]
        self.version = None  # type: Optional[str]
        self.view = None  # type: Optional[str]
        self.promotion = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Images(SOAPElement):
    """
    A container object for holding image assets.
    
    Properties:
    
        - asset (Sequence[Asset]): A sequence of `Asset` objects containing image URLs and metadata.
    """

    elements_properties = OrderedDict([
        ('asset', ('asset', (Asset,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='images',  # type: str
    ):
        self.asset = []  # type: Sequence[Asset]
        super().__init__(xml=xml, tag=tag)


class Product(SOAPElement):
    """    
    Properties:
    
        - action (str): A string indicating the type of operation to be performed. Valid values:
            + "UPDATE"
            + "INSERT"
            + "DELETE"
            + "NO CHANGE"                
        - profiles (Sequence[Profile]): A sequence of ``Profile`` objects containing information about the product.
        - images (Sequence[Asset]): A sequence of ``Asset`` objects containing image URLs and metadata.
        - gtin (str): A 14-digit Global Trade Item Number (GTIN-14).    
        - primary_gtin (str): Used to relate different logistic units to one another. For instance, a consumer unit, 
          case, and palette of a product will all share the same ``primary_gtin``.                   
        - primary_type (str): A string indicating the primary image type. Image assets that have a type matching this 
          value are considered to be the default image for that product.
        - primary_version (str): A string indicating the primary image version. Image assets that have a version  
          matching this value are considered to be the default image for that product. Used in concert with the
          ``primary_type`` property.
        - product_id (int): A unique identifier for the product.
        - upc_10 (str): A 10-digit Global Trade Item Number—also known as a Universal Product Code (UPC-10)
        - upc_12 (str): A 12-digit Global Trade Item Number—also known as a Universal Product Code (UPC-12)
    """

    elements_properties = OrderedDict([
        ('action', ('action', str)),
        ('data/profile', ('profiles', (Profile,))),
        ('gtin', ('gtin', str)),
        ('images/asset', ('images', (Asset,))),
        ('primary_type', ('primary_type', str)),
        ('primary_version', ('primary_version', str)),
        ('product_id', ('product_id', int)),
        ('upc_10', ('upc_10', str)),
        ('upc_12', ('upc_12', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='product',  # type: str
    ):
        self.action = None  # type: Optional[str]
        self.profiles = []  # type: Sequence[Profile]
        self.gtin = None  # type: Optional[str]
        self.images = []  # type: Sequence[Asset]
        self.primary_type = None  # type: Optional[str]
        self.primary_version = None  # type: Optional[str]
        self.product_id = None  # type: Optional[int]
        self.upc_10 = None  # type: Optional[str]
        self.upc_12 = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class KwikeeData(SOAPElement):
    """
    Properties:
    
        - service (Service)
        - products (Sequence[Product]): A container object holding the products returned in response to your request.
        - response (HTTPResponse): This is the `HTTPResponse` object﻿ from which this Kwikee Data was parsed.
    """

    elements_properties = OrderedDict([
        ('service', ('service', Service)),
        ('products/product', ('products', (Product,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='kwikee_data',  # type: str
        service=None,  # type: Optional[Service]
        products=None  # type: Sequence[Product]
    ):
        self.service = service  # type: Service
        self.products = products or []  # type: Sequence[Product]
        super().__init__(xml=xml, tag=tag)
