kwikee
======

A python SDK for retrieving and parsing product data and images using `Kwikee's API`_ (version 1.5).

|
Requirements
------------

- You must have an API user name and password (provided by Kwikee_).
- Python 2.7 or 3.4+

To install:

.. code-block:: python

    pip install kwikee

To follow and execute subsequent code examples, you will need to import the following:

.. code-block:: python

    from kwikee import Kwikee
    from kwikee.elements import KwikeeData, Product
    from datetime import date, timedelta
    from tempfile import gettempdir
    from time import gmtime

The following are only needed for `type hinting`_ in supporting IDE's:

.. code-block:: python

    from typing import Iterator
    from kwikee.elements import Asset
    from http.client import HTTPResponse
|
Authenticate & Connect
----------------------

To connect to Kwikee, create an instance of the *Kwikee* class:

.. code-block:: python

    kwikee_connection = Kwikee(
        user='your_username',
        password='your_password'
    )
|
Retrieving Product Data
-----------------------

Kwikee's API provides replication functionality using two methods, one for initialization (*Kwikee.init*), and one for
incremental updates (*Kwikee.incr*).

To retrieve all products in the Kwikee database:

.. code-block:: python

    kwikee_data = kwikee_connection.init()  # type: Iterator[KwikeeData]

To retrieve incremental updates for a specified time period (in this case the past 30 days):

.. code-block:: python

    kwikee_data = kwikee_connection.incr(
        start_date=date.today() - timedelta(days=30),
        end_date=date.today() - timedelta(days=1)
    )  # type: Iterator[KwikeeData]

Both of these methods will return an iterator of `KwikeeData`_ objects.

|
Interpreting Kwikee Product Data
--------------------------------
.. code-block:: python

    for kd in kwikee_data :  # type: KwikeeData
        for product in kd.products:  # type: Product
            # print an XML representation of this `product`
            print(str(product))

Note: When cast as a *str*—all response elements will return a normalized version of the underlying XML—
suitable for comparison purposes and/or saving as text for later use.

The following table is an overview of elements contained in each response:

======================= ================================================================================================
Class                   **Properties** (*types*)
======================= ================================================================================================
_`KwikeeData`           - **products** (Sequence[`Product`_]): A sequence of products returned in response to
                          your request.
                        - **response** (HTTPResponse_): This is the HTTP response object from which this *KwikeeData*
                          was parsed.
----------------------- ------------------------------------------------------------------------------------------------
_`Product`              - **action** (*str*): A string indicating the type of operation to be performed.

                          + "UPDATE"
                          + "INSERT"
                          + "DELETE"
                          + "NO CHANGE"

                        - **profiles** (Sequence[`Profile`_]): A sequence of `Profile`_ objects containing information
                          about the product.
                        - **images** (Sequence[`Asset`_]): A sequence of `Asset`_ objects containing image URLs and
                          metadata.
                        - **gtin** (*str*): A 14-digit Global Trade Item Number (GTIN-14).
                        - **primary_gtin** (str): Used to relate different logistic units to one another. For instance,
                          a consumer unit, case, and palette of a product will all share the same ``primary_gtin``.
                        - **primary_type** (*str*): A string indicating the primary image type. Image assets that have a
                          type matching this value are considered to be the default image for that product.
                        - **primary_version** (*str*): A string indicating the primary image version. Image assets that
                          have a version matching this value are considered to be the default image for that product.
                          Used in concert with the *primary_type* property.
                        - **product_id** (*int*): A unique identifier for the product.
                        - **upc_10** (*str*): A 10-digit Global Trade Item Number—also known as a Universal Product Code
                          (UPC-10).
                        - **upc_12** (*str*): A 12-digit Global Trade Item Number—also known as a Universal Product Code
                          (UPC-12).
----------------------- ------------------------------------------------------------------------------------------------
_`Profile`              - **action** (*str*): The type of action to perform with the data in this profile.

                          + "UPDATE"
                          + "INSERT"
                          + "DELETE"
                          + "NO CHANGE"

                        - **allergens** (*str*): Allergen warnings.
                        - **brand** (`Brand`_): A container object for the product's brand identification and name.
                        - **case_gtin** (*str*): A 14-digit Global Trade Item Number (GTIN-14) associated with this
                          product's "case" logistics unit.
                        - **case_count** (*str*): The number of consumer units contained in a case of this product.
                        - **case_height** (*str*): The height of this product's "case" logistics unit, expressed in
                          inches.
                        - **case_width** (*str*): The width of this product's "case" logistics unit, expressed in
                          inches.
                        - **case_depth** (*str*): The depth (length measured from front to back) of this product's
                          "case" logistics unit, expressed in inches.
                        - **category** (*str*): A Kwikee shelf management categorization.
                        - **consumable** (*str*): Can I eat this?
                        - **container_type** (*str*): The type of container in which this product is sold to the
                          consumer. Possible values include, but are not limited to: "AEROSOL CAN", "BAG", "BLISTER",
                          "BOTTLE", "BOX", "CAN", "CUP", "GLASS BOTTLE", "JUG", "PACK", "PEG", "PLASTIC BOTTLE",
                          "POUCH", "PUMP", "SLEEVE", "SQUEEZE BOTTLE", "STICK", "TUB", "TUBE", and "WRAPPER".
                        - **custom_product_name** (*str*): A single statement that describes the product as completely
                          as possible for the consumer. This often includes the brand name, product name, size and the
                          container type.
                          Example: "Kraft Dinners Deluxe Original Cheddar Macaroni & Cheese Dinner 14 oz. Box".
                        - **department** (*str*): A Kwikee shelf management categorization.
                        - **depth** (*Decimal*): The depth (length measured from front to back) of the (packaged)
                          consumer unit of this product, expressed in inches.
                        - **height** (*Decimal*): The height of the (packaged) consumer unit of this item, expressed in
                          inches.
                        - **width** (*Decimal*): The width of the (packaged) consumer unit of this item, expressed in
                          inches.
                        - **description** (*str*): A brief description of the product.
                        - **display_depth** (*str*): The depth of the merchandising display for this product.
                        - **display_height** (*str*): The height of the merchandising display for this product.
                        - **display_width** (*str*): The width of the merchandising display for this product.
                        - **division_name** (*str*): A manufacturer-specific categorization.
                        - **division_name_2** (*str*): An additional manufacturer-specific categorization.
                        - **extra_text_2** (*str*)
                        - **extra_text_3** (*str*)
                        - **extra_text_4** (*str*)
                        - **fat_free** (*str*): Flag indicating a claim that a food is fat-free, or any claim likely to
                          have the same meaning for the consumer. May only be made where the product contains no more
                          than 0.5g of fat per 100g or 100ml.
                        - **gluten_free** (*bool*): Flag indicating that the product claims to be gluten free.
                        - **gpc_attributes_assigned** (*datetime*)
                        - **gpc_brick_id** (*str*): Brick is the lowest-level of Global Product Classification (GPC).
                          The brick ID is an 8-digit string composed of integers, representing a unique ID for the
                          classification. For more information, see: http://www.gs1.org/gpc.
                        - **gpc_brick_name** (*str*): Brick is the lowest-level of Global Product Classification (GPC).
                          For more information, see: http://www.gs1.org/gpc.
                        - **gpc_class_id** (*str*): Class is the second-lowest-level of Global Product Classification
                          (GPC). Class ID is an 8-digit string composed of integers, representing a unique ID for the
                          The classification. For more information, see: http://www.gs1.org/gpc.
                        - **gpc_class_name** (*str*): Class is the second-lowest-level of Global Product Classification
                          (GPC). For more information, see: http://www.gs1.org/gpc.
                        - **gpc_family_id** (*str*): Family is the third-lowest-level of Global Product Classification
                          (GPC). The family ID is an 8-digit string composed of integers, representing a unique ID for
                          the classification. For more information, see: http://www.gs1.org/gpc.
                        - **gpc_family_name** (*str*): Family is the third-lowest-level of Global Product Classification
                          (GPC). For more information, see: http://www.gs1.org/gpc.
                        - **gpc_segment_id** (*str*): Segment is the highest level of Global Product Classification
                          (GPC). The segment ID is an 8-digit string composed of integers, representing a unique ID for
                          the classification. For more information, see: http://www.gs1.org/gpc.
                        - **gpc_segment_name** (*str*): Segment is the highest level of Global Product Classification
                          (GPC). For more information, see: http://www.gs1.org/gpc.
                        - **guarantee_analysis** (*str*): A list giving the quantities of various nutrients in a pet
                          food. This is the equivalent of a nutrition panel for pet foods.
                        - **guarantees** (*str*): Promises of satisfaction, compensation or replacement included on a
                          product, such as a warranty or a money-back guarantee.
                        - **indications_copy** (*str*): Information labeled "indications" on drugs.
                        - **ingredients** (*str*): The listed ingredients on foods and beverages, pet foods, or beauty
                          products, the active and inactive ingredients on drugs, or the components of chemicals.
                        - **instructions** (*Sequence[str]*): A sequence of various types of instruction from the
                          product packaging—potentially including: preparation instructions, storage instructions,
                          precautions, or safety information.
                        - **interactions_copy** (*str*): Information labeled "interactions" on drugs.
                        - **kosher** (*str*): Is this product certified as being Kosher?
                        - **legal** (*str*): Contact information for the manufacturer or distributor, information about
                          the country of manufacture, and any legal disclaimers on the package.
                        - **low_fat** (*str*): Flag indicating a claim that a food is low in fat, or any claim likely
                          to have the same meaning for the consumer. May only be made where the product contains no more
                          than 3g of fat per 100g or 1.5g of fat per 100ml.
                        - **manufacturer** (`Manufacturer`_): The name and ID of the product's manufacturer.
                        - **mfr_approved_date** (*datetime*): The date when the manufacturer last approved their image
                          and data assets.
                        - **model** (*str*): The product's model number.
                        - **multiple_shelf_facings** (*bool*): A flag indicating that a product has two or more equaly
                          large advertising surfaces with different orientations.
                        - **nutrition** (`Nutrition`_): Information found on the "Nutrition Facts" panel of a product's
                          packaging.
                        - **organic** (*bool*): A flag indicating whether the product is certified Organic.
                        - **peg_down** (*Decimal*): The measurement from the center of the peg hole to the top of the
                          packaging.
                        - **peg_right** (*Decimal*): The measurement from the center of the peg hole to the left side of
                          the packaging.
                        - **physical_weight_lb** (*Decimal*): The net weight of the product in pounds.
                        - **physical_weight_oz** (*Decimal*): The net weight of the product in ounces.
                        - **post_consumer** (*str*): Information related to the disposal of the product packaging,
                          specifically whether or not the packaging is recyclable, biodegradable, or made from recycled
                          materials.
                        - **product_count** (str): If the product is a multi-pack, or contains multiple distinct
                          elements, such as a 6-pack of soda or a box of 10 fruit snack pouches, this is the number of
                          elements (6 and 10, respectively).
                        - **product_name** (*str*): Product_name answers the question "What is the product?" in one or
                          two words. The simplest descriptor of what the product is.
                        - **product_size** (*Decimal*): The weight, volume, or count that describes the size of the
                          product in concert with the *uom* property which indicates the corresponding unit-of-measure.
                          For example, if the product were a 16 oz. bag of cereal, the *product_size* would be "16".
                        - **profile_id** (*int*): A unique identifier for this profile.
                        - **romance_copy** (`RomanceCopy`_): A container object for detailed information that describes
                          a product or markets the product and/or company to consumers.
                        - **section_id** (*str*): A Kwikee shelf management categorization.
                        - **section_name** (*str*): A Kwikee shelf management categorization.
                        - **supplemental_facts** (`SupplementalFacts`_): Additional information found on the
                          "Supplement Facts" or "Nutrition Facts" panel of a product's packaging. Most products do not
                          have supplemental facts.
                        - **tray_count** (*str*): Number of items in a tray of the product.
                        - **tray_height** (*str*): The height of the tray used to merchandise this product.
                        - **tray_width** (*str*): The width of the tray used to merchandise this product.
                        - **unit_container** (*str*): Any internal container(s) which directly hold the product. For
                          example, if the product were a six-pack of 12 fl. oz. cans of Coke, the Container Type would
                          be CAN.
                        - **unit_size** (*Decimal*): The weight, volume, or count that describes the size of the all
                          individually packaged units within the full product. For example, if the product were a
                          six-pack of 12 fl. oz. cans of Coke, the Unit Size would be 12.
                        - **unit_uom** (*str*): The unit or measurement attached to the Unit Size field. For example,
                          if the product were a six-pack of 12 fl. oz. cans of Coke, the Unit UOM would be "FL OZ".
                        - **uom** (*str*): The unit or measurement attached to the product_size field. For example, if
                          the product were a 16 oz. bag of cereal, the uom would be "OZ".
                        - **warnings_copy** (*str*): Warning or cautionary statements on the package, often preceded by
                          the words "Warning", "Caution", "Hazardous", or "Important".
                        - **why_buy_copy** (`WhyBuyCopy`_): Small specifics on the product packaging (usually the front)
                          that are used to entice a consumer to buy the product.
                        - **nutritional_claims** (`NutritionalClaims`_): A container object for various nutritional
                          claims found on a product's packaging.
                        - **vendor_marketing_bullets** (Sequence[str]): Vendor-provided marketing information intended
                          for presentation as a bullet list.
                        - **vendor_marketing_statements** (Sequence[str]): Vendor-provided marketing statements.
----------------------- ------------------------------------------------------------------------------------------------
_`NutritionalClaims`    - **bpa_free** (*bool*): Is the product BPA free?
                        - **fat_free** (*bool*): Is the product fat free?
                        - **fsc** (*bool*): Is the product Forest Stewardship Council certified?
                        - **halal** (*bool*): Is the product certified halal?
                        - **kosher** (*bool*): Is the product certified kosher?
                        - **low_fat** (*bool*): Is the product low fat?
                        - **non_food** (*bool*): Is the product inedible?
                        - **non_gmo** (*bool*): Is the product GMO free?
                        - **organic** (*bool*): Is the product organic?
                        - **vegan** (*bool*): Is the product vegan?
                        - **vegetarian** (*bool*): Is the product vegetarian?
----------------------- ------------------------------------------------------------------------------------------------
_`Brand`                - **name** (str)
                        - **id** (int)
----------------------- ------------------------------------------------------------------------------------------------
_`Manufacturer`         - **name** (str)
                        - **id** (int)
----------------------- ------------------------------------------------------------------------------------------------
_`RomanceCopy`          - **kwikee_romance** (Sequence[str]): Detailed information that describes a product or markets
                          the product and/or company to consumers. Typically the number of instances is ~3.
----------------------- ------------------------------------------------------------------------------------------------
_`WhyBuyCopy`           - **kwikee_why_buy** (Sequence[str]): A sequence of small specifics on the product packaging
                          (usually the front) that are used to entice a consumer to buy the product.
----------------------- ------------------------------------------------------------------------------------------------
_`Asset`                - **action** (str): Valid values:
                            + "UPDATE"
                            + "INSERT"
                            + "DELETE"
                            + "NO CHANGE"
                        - **asset_id** (int)
                        - **files** (`AssetFiles`_): A container object holding a sequence of file URLs and type
                          (MIME sub-type) indicators such as "JPEG".
                        - **type** (str): Example value: "swatch", for a color/pattern swatch.
                        - **version** (str): Used to identify images of different variations of a product which use the
                          GTIN. For example, color variations such as "red", "green", or "blue".
                        - **view** (str): Code that defines whether the image is color or black & white and also the
                          angle of the image.
----------------------- ------------------------------------------------------------------------------------------------
_`AssetFiles`           - **file** (Sequence[`AssetFile`_]): A sequence of `AssetFile`_ instances, each containing a
                          file type and URL.
----------------------- ------------------------------------------------------------------------------------------------
_`AssetFile`            - **type** (str): The type of file referenced (file extension/MIME sub-type), such as "GS1"
                          (a 2400 x 2400 pixel JPEG image), "JPEG", "JPG", "PNG", "GIF", "TIF", "TIFF", "POG", "TGA",
                          "PSD", "LEPS", "ZIP", "EPS", "WM", "SIT", "LZIP", or "PCX".
                        - **url** (str): The URL from where the referenced image can be retrieved.
----------------------- ------------------------------------------------------------------------------------------------
_`Nutrition`            - **kwikee_nutrition** (Sequence[`KwikeeNutrition`_]): A container object for a sequence of
                          `KwikeeNutrition`_ instances containing information found on the "Nutrition Facts" panel of a
                          product's packaging.
----------------------- ------------------------------------------------------------------------------------------------
_`KwikeeNutrition`      Nutrition quantites and their corresponding units of measure (characterized by the suffix
                        "_uom").

                        - **cal_from_sat_tran_fat** (str)
                        - **calories_per_serving** (str)
                        - **carbo_per_serving** (str)
                        - **carbo_uom** (str)
                        - **cholesterol_per_serving** (str)
                        - **cholesterol_uom** (str)
                        - **fat_calories_per_serving** (str)
                        - **fiber_per_serving** (str)
                        - **fiber_uom** (str)
                        - **insol_fiber_per_serving** (str)
                        - **insol_fiber_per_serving_uom** (str)
                        - **mono_unsat_fat** (str)
                        - **mono_unsat_fat_uom** (str)
                        - **nutrition_label** (str)
                        - **omega_3_polyunsat** (str)
                        - **omega_3_polyunsat_uom** (str)
                        - **omega_6_polyunsat** (str)
                        - **omega_6_polyunsat_uom** (str)
                        - **omega_9_polyunsat** (str)
                        - **omega_9_polyunsat_uom** (str)
                        - **poly_unsat_fat** (str)
                        - **poly_unsat_fat_uom** (str)
                        - **potassium_per_serving** (str)
                        - **potassium_uom** (str)
                        - **protein_per_serving** (str)
                        - **protein_uom** (str)
                        - **sat_fat_per_serving** (str)
                        - **sat_fat_uom** (str)
                        - **serving_size** (str)
                        - **serving_size_uom** (str)
                        - **servings_per_container** (str)
                        - **sodium_per_serving** (str)
                        - **sodium_uom** (str)
                        - **sol_fiber_per_serving** (str)
                        - **sol_fiber_per_serving_uom** (str)
                        - **starch_per_serving** (str)
                        - **starch_per_serving_uom** (str)
                        - **sub_number** (int)
                        - **sugar_per_serving** (str)
                        - **sugar_uom** (str)
                        - **suger_alc_per_serving** (str)
                        - **suger_alc_per_serving_uom** (str)
                        - **total_fat_per_serving** (str)
                        - **total_fat_uom** (str)
                        - **trans_fat_per_serving** (str)
                        - **trans_fat_uom** (str)

                        Nutrient quantities expressed as a percentage of FDA recommended daily values:

                        - **dvp_biotin** (str)
                        - **dvp_calcium** (str)
                        - **dvp_carbo** (str)
                        - **dvp_chloride** (str)
                        - **dvp_cholesterol** (str)
                        - **dvp_chromium** (str)
                        - **dvp_copper** (str)
                        - **dvp_fiber** (str)
                        - **dvp_folic_acid** (str)
                        - **dvp_iodide** (str)
                        - **dvp_iodine** (str)
                        - **dvp_iron** (str)
                        - **dvp_magnesium** (str)
                        - **dvp_manganese** (str)
                        - **dvp_molybdenum** (str)
                        - **dvp_niacin** (str)
                        - **dvp_panthothenate** (str)
                        - **dvp_phosphorus** (str)
                        - **dvp_potassium** (str)
                        - **dvp_protein** (str)
                        - **dvp_riboflavin** (str)
                        - **dvp_sat_tran_fat** (str)
                        - **dvp_saturated_fat** (str)
                        - **dvp_selenium** (str)
                        - **dvp_sodium** (str)
                        - **dvp_sugar** (str)
                        - **dvp_thiamin** (str)
                        - **dvp_total_fat** (str)
                        - **dvp_vitamin_a** (str)
                        - **dvp_vitamin_b12** (str)
                        - **dvp_vitamin_b6** (str)
                        - **dvp_vitamin_c** (str)
                        - **dvp_vitamin_d** (str)
                        - **dvp_vitamin_e** (str)
                        - **dvp_vitamin_k** (str)
                        - **dvp_zinc** (str)
----------------------- ------------------------------------------------------------------------------------------------
_`SupplementalFacts`    - **footers** (Sequence[str])
                        - **footnotes** (Sequence[str])
                        - **headers** (Sequence[str])
                        - **segments** (Sequence[`Segments`_])
----------------------- ------------------------------------------------------------------------------------------------
_`Segments`             - **column_heading** (Sequence[str])
                        - **component_elements** (Sequence[`ComponentElement`_])
----------------------- ------------------------------------------------------------------------------------------------
_`ComponentElement`     - **format** (str)
                        - **name** (str)
                        - **values** (Sequence[str])
======================= ================================================================================================
|
Retrieving Images
-----------------

To retrieve an image, pass the URL of the image to *Kwikee.request*, and parse the resulting HTTPResponse_:

.. code-block:: python

    for kd in kwikee_data :  # type: KwikeeData
        for product in kd.products:  # type: Product
            if product.images is not None:
                for asset in product.images.asset:  # type: Asset
                    for asset_file in asset.files.file:
                        # identify the file name and extension
                        file_name = asset_file.url.split('?')[0].split('/')[-1]
                        extension = file_name.split('.')[-1]
                        file_name = '.'.join(file_name.split('.')[:-1])
                        response = kwikee_connection.request(asset_file.url)  # type: HTTPResponse
                        # write the response data to a file in your temp directory
                        path = '%(directory)s/%(file_name)s_%(timestamp)s.%(extension)s' % dict(
                            directory=gettempdir(),
                            file_name=file_name,
                            timestamp=str(gmtime()),
                            extension=extension
                        )
                        with open(path, mode='wb') as f:
                            f.write(response.file.read())
|
Storing Kwikee Data for Delayed Parsing
---------------------------------------
`KwikeeData`_ instances, as well as all child elements, can be cast as a *str* to obtain an XML representation of the
object, saved in a text file, and reconstituted at a later time by reading the file and initializing an instance of the
appropriate class from *kwikee.elements* with the saved text.

.. code-block:: python

    # save XML text
    i = 1
    kwikee_data_files = []
    product_files = []
    for kd in kwikee_data :  # type: KwikeeData
        # save a copy of this `KwikeeData` instance
        file_name = kd.response.url.split('?')[0].split('/')[-1].replace('.zip', '_%s.xml' % str(i))
        kd_path = '%s/%s' % (
            gettempdir(),
            file_name
        )
        with open(kd_path, mode='w', encoding='utf-8') as f:
            f.write(str(kd))
        kwikee_data_files.append(kd_path)
        kd_dir = kd_path.replace('.xml', '')
        if not os.path.exists(kd_dir):
            os.mkdir(kd_dir)
        for product in kd.products:  # type: Product
            product_path = kd_path.replace('.xml', '/%s.xml' % product.gtin)
            with open(product_path, mode='w', encoding='utf-8') as f:
                f.write(str(product))
            product_files.append(kd_path)
        i += 1
    # delayed parsing
    for kd_path in kwikee_data_files:
        with open(kd_path, mode='r', encoding='utf-8') as f:
            kd_text = f.read()
            kd = KwikeeData(kd_text)
            assert kd_text == str(kd)
    for product_path in product_files:
        with open(product_path, mode='r', encoding='utf-8') as f:
            product_text = f.read()
            product = Product(product_text)
            assert product_text == str(product)
|
References
----------

- `KwikeeSystems API Documentation v1.5`_
- `KwikeeSystems API XSD Field Definitions v1.5`_
- `KwikeeSystems API XSD Summary v1.5`_
- `KwikeeSystems API XSD v1.5`_

..  _`type hinting`: https://www.python.org/dev/peps/pep-0526

..  _`Kwikee's API`: https://www.kwikee.com/cpg-images-data-retailers.shtml#distribute

..  _Kwikee: https://www.kwikee.com/cpg-images-data-retailers.shtml#distribute

..  _HTTPResponse: https://docs.python.org/3/library/http.client.html#httpresponse-objects

..  _`KwikeeSystems API Documentation v1.5`:
    https://docs.google.com/document/d/1nhYEDxRZKX0hSfy0wKeEhbM0s8r1naCFHXYRWPvzYDc

..  _`KwikeeSystems API XSD Field Definitions v1.5`:
    https://docs.google.com/spreadsheets/d/1VxgbKWN3YiGAj3OI9d6ZwUI72_SjS9t-sv3CO6KNc_o

..  _`KwikeeSystems API XSD Summary v1.5`:
    https://docs.google.com/document/d/1C41JOnUDkWHYqvNZEwfGEeSIqK0tL9eRWQOU6YcmE1A

..  _`KwikeeSystems API XSD v1.5`:
    https://api.kwikeesystems.com/api/v1.5/kwikee.xsd

