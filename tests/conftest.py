import os

import pytest

from kwikee import Kwikee

KWIKEE_LOGIN = None if 'KWIKEE_LOGIN' not in os.environ else os.environ['KWIKEE_LOGIN']


def pytest_addoption(parser):
    parser.addoption(
        "--user",
        action="store",
        default=None if KWIKEE_LOGIN is None else KWIKEE_LOGIN.split(':')[0],
        help="user: Kwikee API user name"
    )
    parser.addoption(
        "--password",
        action="store",
        default=None if KWIKEE_LOGIN is None else KWIKEE_LOGIN.split(':')[-1],
        help="password: Kwikee API password"
    )


@pytest.fixture
def kwikee(request):
    credentials = (
        request.config.getoption("--user"),
        request.config.getoption("--password")
    )
    if None in credentials:
        raise EnvironmentError('\n'.join((
            'In order to perform unit tests for this module, please either set the following environment variables ' +
            'to reflect credentials for a current Kwikee API account:',
            '    - "KWIKEE_LOGIN" (in the format "user:password")',
            '...or run pytest with (both of) the following command line options:',
            '    --user',
            '    --password'
        )))
    user, password = credentials
    return Kwikee(
        user=user,
        password=password
    )
