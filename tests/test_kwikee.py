"""
This test module is intended for use with `pytest`, and can be used to confirm that:

    - This package (kwikee) is internally consistent.
    - The SOAP service endpoint referenced by `kwikee.ORIGIN` can be reached, and responds in a format consistent
      with expectations (based on the latest known XSD.
    - This package correctly interprets all data structures contained in responses returned by the the end-point, and
      is therefore presumed to be aligned/up-to-date with the current deployment of the host service.
"""
from __future__ import absolute_import, division, print_function, unicode_literals

from builtins import *
from future import standard_library

standard_library.install_aliases()

from datetime import date, timedelta
from kwikee import KwikeeData, SOAPElement, Kwikee

try:
    from typing import Sequence
except ImportError:
    pass

NoneType = type(None)


def soap_element_test(se):
    # type: (SOAPElement) -> None
    assert isinstance(se, SOAPElement)
    for e, p_t in se.elements_properties.items():
        p, t = p_t
        v = getattr(se, p)
        if isinstance(t, (tuple, list)):
            t = t[0] if t else None
            if t is not None:
                vs = v
                for v in vs:
                    assert(isinstance(v, t))
                    if isinstance(v, SOAPElement):
                        soap_element_test(v)
        else:
            try:
                assert isinstance(v, (t, NoneType))
            except TypeError as ee:
                print(e + ' ' + p + ' ' + repr(t))
                raise ee
            if isinstance(v, SOAPElement):
                soap_element_test(v)


def kwikee_data_test(
    kd  # type: KwikeeData
):
    assert isinstance(kd, KwikeeData)
    assert isinstance(str(kd), str)
    soap_element_test(kd)


def test_kwikee(
    kwikee  # type: Kwikee
):
    data_found = False
    for kd in kwikee.incr(start_date=date.today() - timedelta(days=14)):  # type: KwikeeData
        kwikee_data_test(kd)
        data_found = True
    if not data_found:
        for kd in kwikee.init():  # type: KwikeeData
            soap_element_test(kd)
